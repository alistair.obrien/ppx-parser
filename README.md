# Muon

Muon is a parser generator written in OCaml supporting top-down linear-time parsing with left recursion. 

Grammar specifications are written using a form of mu-regular expressions. 
From the grammar specification, Muon is able to generate:
- The parser
- CST (Concrete Syntax Tree) and AST (Abstract Syntax Tree) types
- A compilation pass from CSTs to ASTs
- Pretty printers

## Documentation

TODO

Please see the [syntax reference]() and [ocamldoc documentation]()


## Quick Start

*Note*: This syntax is not implemented 

Grammars for muon are written using mu-regular expressions (an extension of regular expressions capable of expressing all context-free grammars)
```ocaml
let%token ident =
    { regex = "..."
    ; pp = Fmt.str
    ; lex = (fun lexbuf -> Lexing.lexeme lexbuf)
    }

let%parser function_def 
    = "fn"
    ; @ident
    ; "("
    ; { param &sep:"," }
    ; ")"
    ; [ "->"; type_ ]

and param = @ident; ":"; type_

and type_ = [ ref_kind ]; @ident

and ref_kind = 
    | "&"; "mut" >>| Mutable_reference
    | "&" >>| Immutable_reference
```

Generates the types
```ocaml
module Token =struct
    type nonrec t =
      | Ident of string 
      | Amp 
      | Lpar 
      | Rpar 
      | Comma 
      | Dash_gt 
      | Colon 
      | Fn 
      | Mut 
    
    type nonrec ident = t
    and amp = t
    and lpar = t
    and rpar = t
    and comma = t
    and dash_gt = t
    and colon = t
    and fn = t
    and mut = t

    let ident_to_value =
      function
      | Ident value -> value
      | _ -> failwith "Failed to get value of ident token"
    let pp_ident ppf =
      function
      | Ident value -> Fmt.string ppf value
      | _ -> failwith "Failed to get value of ident token"
end

module Cst = struct
    type function_def =
      Token.fn 
      * Token.ident 
      * Token.lpar 
      * (param, Token.comma) sep_list 
      * Token.rpar 
      * (Token.dash_gt * type_) option

    and param = 
        Token.ident 
        * Token.colon 
        * type_
    
    and ref_kind =
      | Immutable_reference of Token.amp 
      | Mutable_reference of (Token.amp * Token.mut) 
    
    and type_ = ref_kind option * Token.ident
end

module Ast = struct
    type function_def = string * param list * type_ option

    and param = string * type_
    
    and ref_kind =
      | Immutable_reference 
      | Mutable_reference

    and type_ = ref_kind option * string
end

```


