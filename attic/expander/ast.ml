open Core
open Type
open Typed_grammar

let variants t =
  let open Structure in
  let table : (Name.t, Type.t String.Map.t) Hashtbl.t = Hashtbl.create (module Name) in
  let rec loop type_ =
    match Type.structure type_ with
    | Var -> assert false
    | Structure struct_ ->
      (match struct_ with
       | Token | Unit -> ()
       | Tuple tuple -> Tuple.iter ~f:loop tuple
       | Variant { name; tags } ->
         if not (Hashtbl.mem table name)
         then (
           Hashtbl.set table ~key:name ~data:tags;
           Map.iter tags ~f:loop))
  in
  loop t.type_;
  Hashtbl.to_alist table
;;

let rec expand_type ppf type_ =
  let open Structure in
  match Type.structure type_ with
  | Var -> assert false
  | Structure struct_ ->
    (match struct_ with
     | Token -> ()
     | Unit -> ()
     | Tuple tuple ->
       Fmt.pf
         ppf
         "(@[%a@])"
         Fmt.(iter ~sep:(any " *@ ") (fun f t -> Tuple.iter ~f t) expand_type)
         tuple
     | Variant { name; _ } -> Fmt.pf ppf "%s" (Name.name name))
;;

let expand_variants ppf variants =
  let expand_tag ppf (tag, type_) = Fmt.pf ppf "| %s of %a" tag expand_type type_ in
  let expand_variant ppf (name, tags) =
    Fmt.pf
      ppf
      "%s =@.@[<v>%a@]"
      (Name.name name)
      Fmt.(list ~sep:(any "@.") expand_tag)
      (Map.to_alist tags)
  in
  Fmt.pf ppf "type %a" Fmt.(list ~sep:(any "@.and ") expand_variant) variants
;;

module Of_cst = struct
  (* expand_type write code that takes the cst encoding of [type_] *)

  let rec expand_type ppf (var, type_) =
    let open Structure in
    match Type.structure type_ with
    | Var -> assert false
    | Structure struct_ ->
      (match struct_ with
       | Token ->
         (* do nothing with value *)
         ()
       | Unit ->
         (* () -> () (do nothing) *)
         ()
       | Tuple tuple ->
         (* Create pattern variables (ignoring vars for unit) *)
         let cst_var_types =
           List.filter_map tuple.fields ~f:(fun type_ ->
             match Type.structure type_ with
             | Var -> assert false
             | Structure Unit -> None
             | Structure _ -> Some (Var.create (), type_))
         in
         let cst_vars, _ = List.unzip cst_var_types in
         (* Filter Token types *)
         let ast_var_types =
           List.filter cst_var_types ~f:(fun (_, type_) ->
             match Type.structure type_ with
             | Var -> assert false
             | Structure Token -> false
             | _ -> true)
         in
         Fmt.pf
           ppf
           "(let (@[%a@]) = %a in (@[%a@]))"
           Fmt.(list ~sep:(any ",@;") Var.pp)
           cst_vars
           Var.pp
           var
           Fmt.(list ~sep:(any ",@;") expand_type)
           ast_var_types
       | Variant { name; _ } -> Fmt.pf ppf "cst_to_ast_%s" (Name.name name))
  ;;

  let expand_variants ppf variants =
    let expand_tag ppf (tag, type_) =
      let var = Var.create () in
      Fmt.pf ppf "| %s (%a) -> %a" tag Var.pp var expand_type (var, type_)
    in
    let expand_variant ppf (name, tags) =
      Fmt.pf
        ppf
        "cst_to_ast_%s = function@.@[<v>%a@]"
        (Name.name name)
        Fmt.(list ~sep:(any "@.") expand_tag)
        (Map.to_alist tags)
    in
    Fmt.pf ppf "let %a" Fmt.(list ~sep:(any "@.and ") expand_variant) variants
  ;;
end

let generate ppf t =
  let variants = variants t in
  (* Generate Types *)
  expand_variants ppf variants;
  (* Generate Of_cst *)
  Of_cst.expand_variants ppf variants
;;
