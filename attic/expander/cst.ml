open Core
open Type
open Typed_grammar

let variants t =
  let open Structure in
  let table : (Name.t, Type.t String.Map.t) Hashtbl.t = Hashtbl.create (module Name) in
  let rec loop type_ =
    match Type.structure type_ with
    | Var -> assert false
    | Structure struct_ ->
      (match struct_ with
       | Token | Unit -> ()
       | Tuple tuple -> Tuple.iter ~f:loop tuple
       | Variant { name; tags } ->
         if not (Hashtbl.mem table name)
         then (
           Hashtbl.set table ~key:name ~data:tags;
           Map.iter tags ~f:loop))
  in
  loop t.type_;
  Hashtbl.to_alist table
;;

let rec expand_type ppf type_ =
  let open Structure in
  match Type.structure type_ with
  | Var -> assert false
  | Structure struct_ ->
    (match struct_ with
     | Token -> Fmt.pf ppf "Token.t Location.wrap"
     | Unit -> ()
     | Tuple tuple ->
       Fmt.pf
         ppf
         "(@[%a@])"
         Fmt.(iter ~sep:(any " *@ ") (fun f t -> Tuple.iter ~f t) expand_type)
         tuple
     | Variant { name; _ } -> Fmt.pf ppf "%s" (Name.name name))
;;

let expand_variants ppf variants =
  let expand_tag ppf (tag, type_) = Fmt.pf ppf "| %s of %a" tag expand_type type_ in
  let expand_variant ppf (name, tags) =
    Fmt.pf
      ppf
      "%s =@.@[<v>%a@]"
      (Name.name name)
      Fmt.(list ~sep:(any "@.") expand_tag)
      (Map.to_alist tags)
  in
  Fmt.pf ppf "type %a" Fmt.(list ~sep:(any "@.and ") expand_variant) variants
;;

let generate ppf t =
  let variants = variants t in
  expand_variants ppf variants
;;
