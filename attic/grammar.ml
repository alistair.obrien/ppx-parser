open Core

module Var : sig
  type t [@@deriving sexp, compare, hash]

  val pp : t Fmt.t
  val create : unit -> t

  include Comparable.S with type t := t
end = struct
  include Int

  let create =
    let next = ref 0 in
    fun () ->
      Int.incr next;
      !next
  ;;
end

module T = struct
  type t = structure Hash_cons.Elt.t

  and structure =
    | Fail
    | Error of Sexp.t
    | Empty
    | Tok of Token.t
    | Seq of t * t
    | Alt of t * t
    | Var of Var.t
    | Fix of Var.t * t
  [@@deriving sexp, compare, hash]
end

include T

let rec pp : t Fmt.t =
 fun ppf t ->
  match t.contents with
  | Fail -> Fmt.pf ppf "Fail"
  | Error err -> Fmt.pf ppf "@[Error %a@]" Sexp.pp err
  | Empty -> Fmt.pf ppf "Empty"
  | Tok tok -> Token.pp ppf tok
  | Seq (t1, t2) -> Fmt.pf ppf "@[%a%a@]" pp t1 pp t2
  | Alt (t1, t2) -> Fmt.pf ppf "@[(%a)@;|@;(%a)@]" pp t1 pp t2
  | Var var -> Fmt.pf ppf "%a" Var.pp var
  | Fix (var, t) -> Fmt.pf ppf "@[fix %a.@;(%a)@]" Var.pp var pp t
;;
