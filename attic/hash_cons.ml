open Core
module Hashable = Base.Hashtbl.Key

module Elt = struct
  type +'a t =
    { hash : int
    ; contents : 'a
    }
  [@@deriving sexp]

  let create (type a) (hashable : a Hashable.t) (contents : a) : a t =
    let module H = (val hashable : Hashable.S with type t = a) in
    { hash = H.hash contents; contents }
  ;;

  let compare _ t1 t2 = Int.compare t1.hash t2.hash
  let hash_fold_t _ state t = Int.hash_fold_t state t.hash
end

type 'a t =
  { hashable : 'a Hashable.t (* TODO: Ephemeron based API *)
  ; tbl : (int, 'a) Hashtbl.t
  }

let create ?(size = 1000) hashable = { hashable; tbl = Hashtbl.create ~size (module Int) }
let clear t = Hashtbl.clear t.tbl
let copy { hashable; tbl } = { hashable; tbl = Hashtbl.copy tbl }

let iter t ~f =
  Hashtbl.iteri ~f:(fun ~key:hash ~data:contents -> f Elt.{ hash; contents }) t.tbl
;;

let fold t ~f ~init =
  Hashtbl.fold
    t.tbl
    ~f:(fun ~key:hash ~data:contents acc -> f Elt.{ hash; contents } acc)
    ~init
;;

let add { hashable; tbl } contents =
  let elt = Elt.create hashable contents in
  Hashtbl.add_exn tbl ~key:elt.hash ~data:elt.contents;
  elt
;;
