module Hashable = Base.Hashtbl.Key

module Elt : sig
  type +'a t = private
    { hash : int
    ; contents : 'a
    }
  [@@deriving sexp, compare, hash]
end

type 'a t

val create : ?size:int -> 'a Hashable.t -> 'a t
val clear : 'a t -> unit
val copy : 'a t -> 'a t
val iter : 'a t -> f:('a Elt.t -> unit) -> unit
val fold : 'a t -> f:('a Elt.t -> 'b -> 'b) -> init:'b -> 'b
val add : 'a t -> 'a -> 'a Elt.t
