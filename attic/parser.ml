open Core

module Parsetree = struct
  type t =
    { grammar : Grammar.t
    ; input : string
    ; start_pos : int
    ; len : int
    ; children : t list
    }
  [@@deriving sexp, compare, hash]
end

module State = struct
  type t = { grammar : Grammar.compiled }

  let nullable t g = (t.grammar.annotation g).nullable
end

module Memotbl = struct
  module Key = struct
    type t =
      { grammar : Grammar.t
      ; start_pos : int
      }
    [@@deriving sexp, compare, hash]
  end

  module Reduce = struct
    type t =
      { grammar : Grammar.t
      ; start_pos : int
      ; len : int
      ; alt_idx : int option
      ; children : t list
      }

    let create (key : Key.t) ?alt_idx len children =
      { grammar = key.grammar; start_pos = key.start_pos; alt_idx; len; children }
    ;;

    let tok (key : Key.t) len = create key len []
    let empty (key : Key.t) = create key 0 []
    let compare_len t1 t2 = Int.compare t1.len t2.len
  end

  type t =
    { tbl : (Key.t, Reduce.t) Hashtbl.t
    ; input : string
    }

  let create grammar input = { grammar; input; tbl = Hashtbl.create (module Key) }

  let find t key ~state =
    match Hashtbl.find t.tbl key with
    | Some reduce -> Some reduce
    | None -> if State.nullable state key.grammar then Some (Reduce.empty key) else None
  ;;

  let add t ~state ~key ~reduce =
    (* Update [tbl] *)
    let updated = ref false in
    (match Hashtbl.find t.tbl key with
     | Some reduce' ->
       if Reduce.compare_len reduce reduce' > 0
       then (
         updated := true;
         Hashtbl.set t.tbl ~key ~data:reduce)
     | None ->
       updated := true;
       Hashtbl.set t.tbl ~key ~data:reduce);
    List.iter (State.seeds state key.grammar) ~f:(fun seed ->
      if !updated || State.nullable state seed then Queue.enqueue (State.queue state) seed)
  ;;
end
