open Core

module Pos = struct
  type t = int
end

module Input = struct
  type t = Token.t array
end

(* Physical reference *)
module Phys_ref : sig
  type 'a t = private
    { mutable contents : 'a
    ; id : int
    }

  val create : 'a -> 'a t
  val set : 'a t -> 'a -> unit
  val get : 'a t -> 'a
  val hash : 'a t -> int
  val hash_fold_t : 'a t Hash.folder
  val compare : 'a t -> 'a t -> int
end = struct
  type 'a t =
    { mutable contents : 'a
    ; id : int
    }

  let fresh_id =
    let next = ref 0 in
    fun () ->
      Int.incr next;
      !next
  ;;

  let create (contents : 'a) : 'a t = { contents; id = fresh_id () }
  let set t contents = t.contents <- contents
  let get t = t.contents
  let hash t = t.id
  let hash_fold_t state t = Hash.fold_int state t.id
  let compare t1 t2 = Int.compare t1.id t2.id
end

module Memotbl : sig
  type t
end = struct
  module Left_rec = struct end

  (* 
    let rec parser_name = 
      let tbl = Hashtbl.create (module Pos) in 
      fun pos ->
        match Hashtbl.find tbl pos with
        | None -> apply rule
        | 


      if no memo then 
        apply rule  



    type 'a key 

    
  *)

  (* ctx -> (ctx -> 'r) Phys_ref.t -> 'r option *)

  type t
end

module Shared = struct end

module Hashed () : sig
  type 'a i := 'a t

  type 'a t = private
    { hash : int
    ; desc : 'a desc
    }

  and 'a desc =
    | Fail : 'a desc
    | Error : Sexp.t -> 'a desc
    | Empty : 'a -> 'a desc
    | Tok : Token.t -> Token.t desc
    | Seq : 'a t * 'b t -> ('a * 'b) desc
    | Alt : 'a t * 'a t -> 'a desc
    | Fix : 'a Var.t * 'a t -> 'a desc
    | Var : 'a Var.t -> 'a desc
    | Let : 'a Var.t * 'a t * 'b t -> 'a desc
    | Map : 'a t * ('a -> 'b) -> 'b desc

  val hash : 'a t -> int
  val desc : 'a t -> 'a desc

  type iter = { f : 'a. 'a t -> unit }

  val iter : iter -> unit

  type 'b fold = { f : 'a. 'a t -> 'b -> 'b }

  val fold : f:'b fold -> init:'b -> 'b
  val compile : 'a i -> 'a t

  module Private : sig
    module Dyn : sig
      type 'a o := 'a t
      type t

      val of_grammar : 'a o -> t
      val to_grammar : t -> 'a o
    end

    val tbl : (int, Dyn.t) Hashtbl.t
  end
end = struct
  module I = T

  module T = struct
    type 'a t =
      { hash : int
      ; desc : 'a desc
      }

    and 'a desc =
      | Fail : 'a desc
      | Error : Sexp.t -> 'a desc
      | Empty : 'a -> 'a desc
      | Tok : Token.t -> Token.t desc
      | Seq : 'a t * 'b t -> ('a * 'b) desc
      | Alt : 'a t * 'a t -> 'a desc
      | Fix : 'a Var.t * 'a t -> 'a desc
      | Var : 'a Var.t -> 'a desc
      | Let : 'a Var.t * 'a t * 'b t -> 'a desc
      | Map : 'a t * ('a -> 'b) -> 'b desc
  end

  include T

  let hash t = t.hash
  let desc t = t.desc
  let hash_fold_t state t = Hash.fold_int state (hash t)

  let hash_fold_desc : type a. a desc Hash.folder =
   fun state desc ->
    match desc with
    | Fail -> Hash.fold_int state 0
    | Error err ->
      let state = Hash.fold_int state 1 in
      let state = Sexp.hash_fold_t state err in
      state
    | Empty x ->
      let state = Hash.fold_int state 2 in
      let state = Hash.fold_int state (Hashtbl.hash x) in
      state
    | Tok tok ->
      let state = Hash.fold_int state 3 in
      let state = Token.hash_fold_t state tok in
      state
    | Seq (t1, t2) ->
      let state = Hash.fold_int state 4 in
      let state = hash_fold_t state t1 in
      let state = hash_fold_t state t2 in
      state
    | Alt (t1, t2) ->
      let state = Hash.fold_int state 5 in
      let state = hash_fold_t state t1 in
      let state = hash_fold_t state t2 in
      state
    | Fix (var, t) ->
      let state = Hash.fold_int state 6 in
      let state = Var.hash_fold_t state var in
      let state = hash_fold_t state t in
      state
    | Var var ->
      let state = Hash.fold_int state 7 in
      let state = Var.hash_fold_t state var in
      state
    | Let (var, t1, t2) ->
      let state = Hash.fold_int state 8 in
      let state = Var.hash_fold_t state var in
      let state = hash_fold_t state t1 in
      let state = hash_fold_t state t2 in
      state
    | Map (t, f) ->
      let state = Hash.fold_int state 9 in
      let state = hash_fold_t state t in
      let state = Hash.fold_int state (Hashtbl.hash f) in
      state
 ;;

  let hash_desc desc = Hash.of_fold hash_fold_desc desc

  module Dyn : sig
    type 'a grammar := 'a t
    type t

    val of_grammar : 'a grammar -> t
    val to_grammar : t -> 'a grammar
  end = struct
    type t = Obj.t

    let of_grammar grammar = Obj.repr grammar
    let to_grammar t = Obj.obj t
  end

  let tbl : (int, Dyn.t) Hashtbl.t = Hashtbl.create (module Int)

  let add desc =
    let hash = hash_desc desc in
    let elt = { hash; desc } in
    Hashtbl.add_exn tbl ~key:hash ~data:(Dyn.of_grammar elt);
    elt
  ;;

  module O = T

  let rec compile : type a. a I.t -> a O.t =
   fun t ->
    let self t = compile t in
    let return desc = add desc in
    match t with
    | Fail -> return Fail
    | Error err -> return (Error err)
    | Empty x -> return (Empty x)
    | Tok tok -> return (Tok tok)
    | Seq (t1, t2) ->
      let t1 = self t1
      and t2 = self t2 in
      return (Seq (t1, t2))
    | Alt (t1, t2) ->
      let t1 = self t1
      and t2 = self t2 in
      return (Alt (t1, t2))
    | Var var -> return (Var var)
    | Fix (var, t) ->
      let t = self t in
      return (Fix (var, t))
    | Map (t, f) ->
      let t = self t in
      return (Map (t, f))
    | Let (var, t1, t2) ->
      let t1 = self t1
      and t2 = self t2 in
      return (Let (var, t1, t2))
 ;;

  type iter = { f : 'a. 'a t -> unit }

  let iter it = Hashtbl.iter tbl ~f:(fun dyn -> it.f @@ Dyn.to_grammar dyn)

  type 'b fold = { f : 'a. 'a t -> 'b -> 'b }

  let fold ~f:fl ~init =
    Hashtbl.fold tbl ~f:(fun ~key:_ ~data:dyn acc -> fl.f (Dyn.to_grammar dyn) acc) ~init
  ;;

  module Private = struct
    module Dyn = Dyn

    let tbl = tbl
  end
end

module Pracket () = struct
  module Hashed = Hashed ()

  module Input = struct
    type t = Token.t array
  end

  module Pos = struct
    type t = int [@@deriving hash]
  end

  module Memotbl : sig
    type t

    type 'a result =
      [ `Ok of 'a * Pos.t
      | `Fail
      ]

    val create : ?size:int -> unit -> t
    val add : t -> 'a Hashed.t -> Pos.t -> 'a result -> unit
    val find : t -> 'a Hashed.t -> Pos.t -> 'a result option
  end = struct
    type 'a result =
      [ `Ok of 'a * Pos.t
      | `Fail
      ]

    let map_result t ~f =
      match t with
      | `Ok (x, pos) -> `Ok (f x, pos)
      | `Fail -> `Fail
    ;;

    type t = (int, Obj.t result) Hashtbl.t

    let create ?size () = Hashtbl.create ?size (module Int)
    let hash hashed pos = [%hash: int * int] (Hashed.hash hashed, Pos.hash pos)

    let add t hashed pos result =
      Hashtbl.set t ~key:(hash hashed pos) ~data:(map_result result ~f:Obj.repr)
    ;;

    let find t hashed pos =
      Hashtbl.find t (hash hashed pos) |> Option.map ~f:(map_result ~f:Obj.obj)
    ;;
  end

  module Ctx = Var.Map (Hashed)

  let parse t ~input =
    let tbl = Memotbl.create () in
    let rec loop : type a. a Hashed.t -> ctx:Ctx.t -> Pos.t -> a Memotbl.result =
     fun t ~ctx pos ->
      let self t ?(ctx = ctx) pos = loop t ~ctx pos in
      match Memotbl.find tbl t pos with
      | Some ret -> ret
      | None ->
        let ret =
          match t.desc with
          | Fail -> `Fail
          | Error _err -> `Fail
          | Empty x -> `Ok (x, pos)
          | Tok tok ->
            (match Array.get input pos with
             | tok' when Token.(tok = tok') -> `Ok (tok, pos + 1)
             | _ -> `Fail)
          | Seq (t1, t2) ->
            (match self t1 pos with
             | `Fail -> `Fail
             | `Ok (val1, pos) ->
               (match self t2 pos with
                | `Fail -> `Fail
                | `Ok (val2, pos) -> `Ok ((val1, val2), pos)))
          | Alt (t1, t2) ->
            (match self t1 pos with
             | `Fail -> self t2 pos
             | _ as ret -> ret)
          | Var var ->
            (match Ctx.find ctx var with
             | None -> raise_s [%message "Unbound variable" (var : Var.t)]
             | Some t -> self t pos)
          | Fix (var, t) -> self t ~ctx:(Ctx.add ctx var t) pos
          | Map (t, f) ->
            (match self t pos with
             | `Ok (val_, pos) -> `Ok (f val_, pos)
             | `Fail -> `Fail)
          | Let _ -> assert false
        in
        Memotbl.add tbl t pos ret;
        ret
    in
    loop (Hashed.compile t) ~ctx:Ctx.empty 0
  ;;
end
