[@@@warning "-32"]

open Ppxlib
open Muon

let loc = Location.none

open Grammar
module S = Structure

let calc_example =
  let int_tok =
    S.let_tok
      ~semantics:
        { type_ = Type.constr0 "int"
        ; lex = [%expr fun lexbuf -> Int.to_string (Lexing.lexeme lexbuf)]
        ; pp = [%expr Fmt.int]
        }
      "int"
      "..."
  in
  let _expr, expr_def =
    S.let_
    @@ let_rec
         (fun expr ->
           annot ~type_:Type.(hierarchy (constr0 "expr") 3)
           @@ hierarchy
                [ [ "E_plus", seq [ expr.!{0}; tokc "+"; expr.!{1} ] ]
                ; [ "E_mult", seq [ expr.!{1}; tokc "*"; expr.!{2} ] ]
                ; [ "E_num", tokn "int"
                  ; "E_paren", seq [ tokc "("; expr.!{0}; tokc ")" ]
                  ]
                ])
         ~in_:(fun expr -> expr.!{0})
  in
  [ int_tok; expr_def ]
;;

let readme_example =
  let ident_tok =
    S.let_tok
      ~semantics:
        { type_ = Type.constr0 "string"
        ; lex = [%expr fun lexbuf -> Int.to_string (Lexing.lexeme lexbuf)]
        ; pp = [%expr Fmt.int]
        }
      "ident"
      "..."
  in
  let ref_kind, ref_kind_def =
    S.let_
    @@ annot ~type_:(Type.constr0 "ref_kind")
    @@ alt
         [ "Mutable_reference", seq [ tokc "&"; tokc "mut" ]
         ; "Immutable_reference", tokc "&"
         ]
  in
  let type_, type_def =
    S.let_
    @@ annot ~type_:(Type.constr0 "type_")
    @@ seq [ optional ref_kind; tokn "ident" ]
  in
  let param, param_def =
    S.let_ @@ annot ~type_:(Type.constr0 "param") @@ seq [ tokn "ident"; tokc ":"; type_ ]
  in
  let _fun_def, fun_def_def =
    S.let_
    @@ annot ~type_:(Type.constr0 "function_def")
    @@ seq
         [ tokc "fn"
         ; tokn "ident"
         ; tokc "("
         ; sep_list param ~sep:(tokc ",")
         ; tokc ")"
         ; optional (seq [ tokc "->"; type_ ])
         ]
  in
  [ ident_tok; ref_kind_def; type_def; param_def; fun_def_def ]
;;

(* imp language

  C ::= x := e | if b B else B | while b B
  B ::= { C1; ... ; Cn }
  b ::= true | false | b && b | b || b | e = e
  e ::= int | (e) | e + e | e * e | x
*)

let imp_example =
  let ident_tok =
    S.let_tok
      ~semantics:
        { type_ = Type.constr0 "string"
        ; lex = [%expr fun lexbuf -> Int.to_string (Lexing.lexeme lexbuf)]
        ; pp = [%expr Fmt.int]
        }
      "ident"
      "..."
  in
  let int_tok =
    S.let_tok
      ~semantics:
        { type_ = Type.constr0 "int"
        ; lex = [%expr fun lexbuf -> Int.to_string (Lexing.lexeme lexbuf)]
        ; pp = [%expr Fmt.int]
        }
      "int"
      "..."
  in
  let expr, expr_def =
    S.let_rec (fun expr ->
      annot ~type_:Type.(hierarchy (constr0 "expr") 3)
      @@ hierarchy
           [ [ "E_plus", seq [ expr.!{0}; tokc "+"; expr.!{1} ] ]
           ; [ "E_mult", seq [ expr.!{1}; tokc "*"; expr.!{2} ] ]
           ; [ "E_num", tokn "int"
             ; "E_ident", tokn "ident"
             ; "E_paren", seq [ tokc "("; expr.!{0}; tokc ")" ]
             ]
           ])
  in
  let bool_expr, bool_expr_def =
    S.let_rec (fun bool_expr ->
      annot ~type_:Type.(hierarchy (constr0 "bool_expr") 4)
      @@ hierarchy
           [ [ "B_or", seq [ bool_expr.!{0}; tokc "||"; bool_expr.!{1} ] ]
           ; [ "B_and", seq [ bool_expr.!{1}; tokc "&&"; bool_expr.!{2} ] ]
           ; [ "B_eq", seq [ expr.!{0}; tokc "="; expr.!{0} ] ]
           ; [ "B_true", tokc "true"
             ; "B_false", tokc "false"
             ; "B_paren", seq [ tokc "("; bool_expr.!{0}; tokc ")" ]
             ]
           ])
  in
  let _cmd, cmd_def =
    S.let_rec (fun cmd ->
      annot ~type_:(Type.constr0 "cmd")
      @@ let_
           (annot ~type_:(Type.constr0 "block")
           @@ seq [ tokc "{"; sep_list cmd ~sep:(tokc ";"); tokc "}" ])
           ~in_:(fun block ->
             alt
               [ "C_assign", seq [ tokn "ident"; tokc ":="; expr.!{0} ]
               ; "C_if", seq [ tokc "if"; bool_expr.!{0}; block; tokc "else"; block ]
               ; "C_while", seq [ tokc "while"; bool_expr.!{0}; block ]
               ]))
  in
  [ ident_tok; int_tok; expr_def; bool_expr_def; cmd_def ]
;;
