open Core
open Typing
open Ppxlib

(* TODO: (After PoC) Remove reliance on [Location.none] *)

open (val Ast_builder.make Location.none)

let none t = { txt = t; loc = Location.none }

module Ident = struct
  let create_id =
    let next = ref 0 in
    fun () ->
      let id = !next in
      Int.incr next;
      id
  ;;

  let create name =
    let id = create_id () in
    [%string "%{name}_%{id#Int}"]
  ;;

  let lident name =
    let ident = create name in
    none @@ Lident ident
  ;;

  let pat ?(used = true) name =
    let ident = create name in
    if used then ppat_var (none ident) else ppat_var (none ("_" ^ ident))
  ;;

  let both name =
    let ident = create name in
    ppat_var (none ident), none @@ Lident ident
  ;;
end

module Cst = struct
  (* [transl_type type_] translates [type_] to a [core_type] (naively) *)
  let rec transl_type (type_ : Type.t) =
    match type_ with
    | Unit -> ptyp_constr (none @@ Lident "unit") []
    | Tok tok -> ptyp_constr (none @@ Ldot (Lident "Token", Token.to_alphanum tok)) []
    | Constr { constr; params } ->
      ptyp_constr (none @@ Lident constr) (List.map ~f:transl_type params)
    | Tuple tuple -> ptyp_tuple (List.map ~f:transl_type tuple)
    | Hierarchy (t, _) -> transl_type t
  ;;

  let def_type_decl type_decl =
    let { Type.constr; kind } = type_decl in
    let manifest, kind =
      match kind with
      | Variant tags ->
        let constr_decls =
          tags
          |> Map.to_alist
          |> List.map ~f:(fun (tag, type_) ->
               constructor_declaration
                 ~name:(none tag)
                 ~res:None
                 ~args:(Pcstr_tuple [ transl_type type_ ]))
        in
        None, Ptype_variant constr_decls
      | Alias type_ -> Some (transl_type type_), Ptype_abstract
    in
    type_declaration
      ~name:(none constr)
      ~params:[]
      ~cstrs:[]
      ~private_:Public
      ~manifest
      ~kind
  ;;

  let expand type_decls =
    let type_decls = type_decls |> Map.data |> List.map ~f:def_type_decl in
    pstr_type Recursive type_decls
  ;;

  module Pretty_printer = struct
    let muon_fmt fun_ args =
      pexp_apply
        (pexp_ident (none @@ Ldot (Lident "Muon_fmt", fun_)))
        (List.map ~f:(fun arg -> Nolabel, arg) args)
    ;;

    let pp_tok name ppf tok =
      pexp_apply
        (pexp_ident (none @@ Ldot (Lident "Token", "pp_" ^ name)))
        [ Nolabel, ppf; Nolabel, tok ]
    ;;

    let seq exps =
      match exps with
      | exp :: exps ->
        List.fold_left exps ~init:exp ~f:(fun exp exp' -> pexp_sequence exp exp')
      | [] -> assert false
    ;;

    let unit = pexp_ident (none @@ Lident "()")

    (* [pp] has a (int -> unit) type (ppf + arg is partially applied) *)
    let enter pp level =
      pexp_apply
        pp
        [ Nolabel, pexp_constant (Pconst_integer (Int.to_string level, None)) ]
    ;;

    let prec curr_level level pp_fun ppf x =
      muon_fmt
        "prec"
        [ curr_level
        ; pexp_constant (Pconst_integer (Int.to_string level, None))
        ; pp_fun
        ; ppf
        ; x
        ]
    ;;

    let rec pp ppf (g : Typed_grammar.t) =
      match g.content with
      | Tok (Named name as tok) ->
        let pat, lident = Ident.both (Token.to_alphanum tok) in
        pat, pp_tok name ppf (pexp_ident lident)
      | Tok (Constant const as tok) ->
        let pat = Ident.pat ~used:false (Token.to_alphanum tok) in
        ( pat
        , muon_fmt
            "str"
            [ ppf; pexp_constant (Pconst_string (const, Location.none, None)) ] )
      | Seq gs ->
        let pats, pps = gs |> List.map ~f:(pp ppf) |> List.unzip in
        ppat_tuple pats, seq pps
      | Alt alt ->
        let cases =
          List.map alt ~f:(fun (tag, g) ->
            let pat, pp = pp ppf g in
            let constr_pat = ppat_construct (none @@ Lident tag) (Some pat) in
            case ~lhs:constr_pat ~guard:None ~rhs:pp)
        in
        let pat, lident = Ident.both "alt" in
        pat, pexp_match (pexp_ident lident) cases
      | Optional g ->
        let pat, lident = Ident.both "opt" in
        let pp_fun = pp_fun g in
        pat, muon_fmt "opt" [ pp_fun; ppf; pexp_ident lident ]
      | List g ->
        let pat, lident = Ident.both "list" in
        let pp_fun = pp_fun g in
        pat, muon_fmt "list" [ pp_fun; ppf; pexp_ident lident ]
      | Sep_list (g1, g2) ->
        let pat, lident = Ident.both "sep_list" in
        let pp_fun1 = pp_fun g1 in
        let pp_fun2 = pp_fun g2 in
        pat, muon_fmt "sep_list" [ pp_fun1; pp_fun2; ppf; pexp_ident lident ]
      | Enter (g, level) ->
        let pat, pp = pp ppf g in
        pat, enter pp level
      | Hierarchy levels ->
        let curr_level_pat, curr_level_lident = Ident.both "curr_level" in
        let pat, lident = Ident.both "hier" in
        let cases =
          levels
          |> Map.to_alist
          |> List.concat_map ~f:(fun (level, alt) ->
               List.map alt ~f:(fun (tag, grammar) -> tag, level, grammar))
          |> List.map ~f:(fun (tag, level, g) ->
               let pat, lident = Ident.both "x" in
               let pp_fun = pp_fun g in
               let rhs =
                 prec (pexp_ident curr_level_lident) level pp_fun ppf (pexp_ident lident)
               in
               let pat = ppat_construct (none @@ Lident tag) (Some pat) in
               case ~lhs:pat ~guard:None ~rhs)
        in
        let match_ = pexp_match (pexp_ident lident) cases in
        pat, pexp_fun Nolabel None curr_level_pat match_
      | Var var ->
        let pat, lident = Ident.both "x" in
        ( pat
        , pexp_apply
            (pexp_ident (none @@ Lident (Var.to_alphanum var)))
            [ Nolabel, ppf; Nolabel, pexp_ident lident ] )
      | Let { binding = var1, g1; in_ = g2 } ->
        let pp_fun1 = pp_fun g1 in
        let pat2, pp2 = pp ppf g2 in
        ( pat2
        , pexp_let
            Nonrecursive
            [ value_binding ~pat:(ppat_var (none @@ Var.to_alphanum var1)) ~expr:pp_fun1 ]
            pp2 )
      | Let_rec { bindings; in_ = g2 } ->
        let bindings =
          List.map bindings ~f:(fun (var, g) ->
            value_binding ~pat:(ppat_var (none @@ Var.to_alphanum var)) ~expr:(pp_fun g))
        in
        let pat2, pp2 = pp ppf g2 in
        pat2, pexp_let Recursive bindings pp2

    and pp_fun g =
      let ppf_pat, ppf = Ident.both "ppf" in
      let pat, exp = pp (pexp_ident ppf) g in
      (* TODO: Eta-expansion optimisation *)
      pexp_fun Nolabel None ppf_pat @@ pexp_fun Nolabel None pat exp
    ;;

    let expand (str : Typed_grammar.Structure.t) =
      let value_binding (var, g) =
        value_binding
          ~pat:(ppat_var (none @@ "pp_" ^ Var.to_alphanum var))
          ~expr:(pp_fun g)
      in
      let str_items =
        List.filter_map str ~f:(function
          | Let binding -> Some (pstr_value Nonrecursive [ value_binding binding ])
          | Let_rec bindings ->
            Some (pstr_value Nonrecursive (List.map bindings ~f:value_binding))
          | _ -> None)
      in
      str_items
    ;;
  end
end

module Token_ = struct
  (* [const_toks_*] traverses the grammar discovering used constant tokens *)

  let rec const_toks (grammar : Typed_grammar.t) =
    let module Set = String.Set in
    match grammar.content with
    | Tok (Constant const) -> Set.singleton const
    | Tok (Named _) -> Set.empty
    | Alt alt -> const_toks_alt alt
    | Seq ts -> ts |> List.map ~f:const_toks |> Set.union_list
    | Optional t | List t -> const_toks t
    | Sep_list (t1, t2) -> Set.union (const_toks t1) (const_toks t2)
    | Let_rec { bindings; in_ } ->
      Set.union (const_toks_bindings bindings) (const_toks in_)
    | Let { binding; in_ } -> Set.union (const_toks_binding binding) (const_toks in_)
    | Var _ -> Set.empty
    | Hierarchy levels -> Map.data levels |> List.map ~f:const_toks_alt |> Set.union_list
    | Enter (t, _) -> const_toks t

  and const_toks_alt alt =
    alt |> List.map ~f:(fun (_tag, t) -> const_toks t) |> String.Set.union_list

  and const_toks_binding (_, t) = const_toks t

  and const_toks_bindings bindings =
    bindings |> List.map ~f:const_toks_binding |> String.Set.union_list

  and const_toks_str_item (str_item : Typed_grammar.Structure.item) =
    match str_item with
    | Let binding -> const_toks_binding binding
    | Let_rec bindings -> const_toks_bindings bindings
    | Let_tok _ -> String.Set.empty

  and const_toks_str str = str |> List.map ~f:const_toks_str_item |> String.Set.union_list

  (* [named_toks grammar] returns a list of non-unit named tokens *)
  let named_toks (str : Typed_grammar.Structure.t) =
    List.filter_map str ~f:(function
      | Let_tok { name; semantics; _ } -> Some (name, semantics)
      | _ -> None)
  ;;

  let expand_type_decl tokens =
    let variant_decls =
      List.map tokens ~f:(fun (tag, type_) ->
        let tag = String.capitalize tag in
        let args = type_ |> Option.map ~f:Cst.transl_type |> Option.to_list in
        constructor_declaration ~name:(none tag) ~res:None ~args:(Pcstr_tuple args))
    in
    type_declaration
      ~name:(none "t")
      ~params:[]
      ~cstrs:[]
      ~private_:Public
      ~manifest:None
      ~kind:(Ptype_variant variant_decls)
  ;;

  let expand_type_aliases tokens =
    let aliases =
      List.map tokens ~f:(fun (token, _) ->
        type_declaration
          ~name:(none token)
          ~params:[]
          ~private_:Public
          ~manifest:(Some (ptyp_constr (none @@ Lident "t") []))
          ~cstrs:[]
          ~kind:Ptype_abstract)
    in
    aliases
  ;;

  let expand_pps named_tokens =
    List.filter_map named_tokens ~f:(fun (name, semantics) ->
      match semantics with
      | Some Grammar.Structure.{ pp; _ } ->
        let fun_ =
          pexp_fun Nolabel None (ppat_var (none "ppf"))
          @@ pexp_function
               [ (* [Name (value) -> pp ppf value] *)
                 case
                   ~lhs:
                     (ppat_construct
                        (none @@ Lident (String.capitalize name))
                        (Some (ppat_var (none "value"))))
                   ~guard:None
                   ~rhs:
                     (pexp_apply
                        pp
                        [ Nolabel, pexp_ident (none @@ Lident "ppf")
                        ; Nolabel, pexp_ident (none @@ Lident "value")
                        ])
               ; (* [_ -> failwith "Failed to get value of %{name} token"]*)
                 case
                   ~lhs:ppat_any
                   ~guard:None
                   ~rhs:
                     (pexp_apply
                        (pexp_ident (none @@ Lident "failwith"))
                        [ ( Nolabel
                          , pexp_constant
                              (Pconst_string
                                 ( [%string "Failed to get value of %{name} token"]
                                 , Location.none
                                 , None )) )
                        ])
               ]
        in
        Some
          (pstr_value
             Nonrecursive
             [ value_binding ~pat:(ppat_var (none @@ [%string "pp_%{name}"])) ~expr:fun_ ])
      | None -> None)
  ;;

  let expand_value_convs named_tokens =
    List.filter_map named_tokens ~f:(fun (name, type_) ->
      match type_ with
      | Some _ ->
        let fun_ =
          pexp_function
            [ (* [Name (value) -> value] *)
              case
                ~lhs:
                  (ppat_construct
                     (none @@ Lident (String.capitalize name))
                     (Some (ppat_var (none "value"))))
                ~guard:None
                ~rhs:(pexp_ident (none @@ Lident "value"))
            ; (* [_ -> failwith "Failed to get value of %{name} token"]*)
              case
                ~lhs:ppat_any
                ~guard:None
                ~rhs:
                  (pexp_apply
                     (pexp_ident (none @@ Lident "failwith"))
                     [ ( Nolabel
                       , pexp_constant
                           (Pconst_string
                              ( [%string "Failed to get value of %{name} token"]
                              , Location.none
                              , None )) )
                     ])
            ]
        in
        Some
          (pstr_value
             Nonrecursive
             [ value_binding
                 ~pat:(ppat_var (none @@ [%string "%{name}_to_value"]))
                 ~expr:fun_
             ])
      | None -> None)
  ;;

  (* Generate functions used to forget tokens *)

  let expand str =
    let named_tokens = named_toks str in
    let value_convs = expand_value_convs named_tokens in
    let pps = expand_pps named_tokens in
    (* Create full list of tokens *)
    let tokens =
      let named_tokens =
        List.map named_tokens ~f:(fun (name, semantics) ->
          ( Token.to_alphanum (Named name)
          , Option.map ~f:(fun semantics -> semantics.type_) semantics ))
      in
      let const_tokens =
        const_toks_str str
        |> Set.to_list
        |> List.map ~f:(fun const -> Token.to_alphanum (Constant const), None)
      in
      named_tokens @ const_tokens
    in
    let type_decl = expand_type_decl tokens in
    let type_aliases = expand_type_aliases tokens in
    pstr_module
      (module_binding
         ~name:(none @@ Some "Token")
         ~expr:
           (pmod_structure
              ([ pstr_type Nonrecursive [ type_decl ]
               ; pstr_type Nonrecursive type_aliases
               ]
              @ value_convs
              @ pps)))
  ;;
end

module Ast = struct
  (* [transl_type type_] translates [type_] to a [core_type] (forgetfully) *)
  let rec transl_type tokens (type_ : Type.t) =
    let open Option.Let_syntax in
    let transl_type = transl_type tokens in
    match type_ with
    | Unit ->
      (* We forget [Unit] for the AST since it provides no useful information *)
      None
    | Tok (Named name) when Option.is_some (Map.find_exn tokens name) ->
      (* We don't forget tokens that are named *and* have a semantic value attached *)
      let type_ = Map.find_exn tokens name |> Option.value_exn in
      transl_type type_
    | Tok _ ->
      (* We forget constant tokens that are either constant or named but have no semantic value *)
      None
    | Constr { constr = "sep_list"; params = [ t1; t2 ] } ->
      (* if [t2] is forgetten and t1 isn't => t1 list *)
      let%bind t1 = transl_type t1 in
      (match transl_type t2 with
       | None -> return @@ ptyp_constr (none @@ Lident "list") [ t1 ]
       | Some t2 -> return @@ ptyp_constr (none @@ Lident "sep_list") [ t1; t2 ])
    | Constr { constr; params } ->
      (* A constructor is forgetten iff a parameter is forgetten *)
      let%bind params = params |> List.map ~f:transl_type |> Option.all in
      return @@ ptyp_constr (none @@ Lident constr) params
    | Tuple tuple ->
      (* Forgetten components of a tuple are simply dropped. *)
      let tuple = List.filter_map tuple ~f:transl_type in
      return @@ ptyp_tuple tuple
    | Hierarchy (t, _) -> transl_type t
  ;;

  let def_type_decl tokens type_decl =
    let { Type.constr; kind } = type_decl in
    let manifest, kind =
      match kind with
      | Variant tags ->
        let constr_decls =
          tags
          |> Map.to_alist
          |> List.map ~f:(fun (tag, type_) ->
               constructor_declaration
                 ~name:(none tag)
                 ~res:None
                 ~args:(Pcstr_tuple (Option.to_list @@ transl_type tokens type_)))
        in
        None, Ptype_variant constr_decls
      | Alias type_ ->
        Some (Option.value_exn ~here:[%here] @@ transl_type tokens type_), Ptype_abstract
    in
    type_declaration
      ~name:(none constr)
      ~params:[]
      ~cstrs:[]
      ~private_:Public
      ~manifest
      ~kind
  ;;

  let expand tokens type_decls =
    let type_decls = type_decls |> Map.data |> List.map ~f:(def_type_decl tokens) in
    pstr_type Recursive type_decls
  ;;

  module Of_cst = struct
    (* [conv type_] creates a conversion for the type [Cst.transl_type type_] to [Ast.transl_type type_] *)
    let rec conv tokens (type_ : Type.t) =
      let conv = conv tokens in
      let conv_fun = conv_fun tokens in
      match type_ with
      | Unit -> ppat_construct (none @@ Lident "()") None, None
      | Tok (Named name as tok) when Option.is_some (Map.find_exn tokens name) ->
        let pat, lident = Ident.both (Token.to_alphanum tok) in
        ( pat
        , Some
            (pexp_apply
               (pexp_ident (none @@ Ldot (Lident "Token", [%string "%{name}_to_value"])))
               [ Nolabel, pexp_ident lident ]) )
      | Tok tok ->
        let pat = Ident.pat ~used:false (Token.to_alphanum tok) in
        pat, None
      | Constr { constr = "sep_list"; params = [ t1; t2 ] } ->
        (match conv_fun t1, conv_fun t2 with
         | None, _ ->
           let pat = Ident.pat ~used:false "sep_list" in
           pat, None
         | Some conv1, None ->
           let pat, lident = Ident.both "sep_list" in
           ( pat
           , Some
               (pexp_apply
                  (pexp_ident (none @@ Lident "sep_list1"))
                  [ Nolabel, conv1; Nolabel, pexp_ident lident ]) )
         | Some conv1, Some conv2 ->
           let pat, lident = Ident.both "sep_list" in
           ( pat
           , Some
               (pexp_apply
                  (pexp_ident (none @@ Lident "sep_list2"))
                  [ Nolabel, conv1; Nolabel, conv2; Nolabel, pexp_ident lident ]) ))
      | Constr { constr; params } ->
        (match params |> List.map ~f:conv_fun |> Option.all with
         | Some convs ->
           let pat, lident = Ident.both constr in
           ( pat
           , Some
               (pexp_apply
                  (pexp_ident (none @@ Lident constr))
                  (List.map ~f:(fun conv -> Nolabel, conv) convs
                  @ [ Nolabel, pexp_ident lident ])) )
         | None ->
           let pat = Ident.pat ~used:false constr in
           pat, None)
      | Tuple tuple ->
        let tuple_pat, tuple_exps = tuple |> List.map ~f:conv |> List.unzip in
        ppat_tuple tuple_pat, Some (pexp_tuple (List.filter_opt tuple_exps))
      | Hierarchy (t, _) -> conv t

    and conv_fun tokens type_ =
      let pat, exp = conv tokens type_ in
      match exp with
      | None -> None
      | Some exp ->
        (* TODO: Eta-expansion optimisation *)
        Some (pexp_fun Nolabel None pat exp)
    ;;

    let def_type_decl tokens type_decl =
      let { Type.constr; kind } = type_decl in
      value_binding
        ~pat:(ppat_var (none constr))
        ~expr:
          (match kind with
           | Variant tags ->
             let cases =
               tags
               |> Map.to_alist
               |> List.map ~f:(fun (tag, type_) ->
                    let pat, exp = conv tokens type_ in
                    let pat = ppat_construct (none @@ Lident tag) (Some pat) in
                    case
                      ~lhs:pat
                      ~guard:None
                      ~rhs:(pexp_construct (none @@ Lident tag) exp))
             in
             pexp_function cases
           | Alias type_ -> conv_fun tokens type_ |> Option.value_exn ~here:[%here])
    ;;

    let expand tokens type_decls =
      let value_bindings = type_decls |> Map.data |> List.map ~f:(def_type_decl tokens) in
      pstr_value Recursive value_bindings
    ;;
  end
end

let expand_tokens ~ctx:_ str = Token_.expand str

let expand_cst ~ctx str =
  let type_decls = Ctx.type_decls ctx in
  let cst_types = Cst.expand type_decls in
  let pps = Cst.Pretty_printer.expand str in
  pstr_module
    (module_binding ~name:(none @@ Some "Cst") ~expr:(pmod_structure (cst_types :: pps)))
;;

let expand_ast ~ctx _str =
  let type_decls = Ctx.type_decls ctx in
  let tokens = Ctx.tokens ctx in
  let ast_types = Ast.expand tokens type_decls in
  let conv =
    let convs = Ast.Of_cst.expand tokens type_decls in
    pstr_module
      (module_binding ~name:(none @@ Some "Of_cst") ~expr:(pmod_structure [ convs ]))
  in
  pstr_module
    (module_binding ~name:(none @@ Some "Ast") ~expr:(pmod_structure [ ast_types; conv ]))
;;

let expand str =
  let ctx = Ctx.empty () in
  let ctx, typed_str = infer_str ~ctx str in
  [ expand_tokens ~ctx typed_str; expand_cst ~ctx typed_str; expand_ast ~ctx typed_str ]
;;