open Ppxlib
open Core

module Var : sig
  type t

  include Identifiable.S with type t := t

  val pp : t Fmt.t
  val create : unit -> t
  val to_alphanum : t -> string
end = struct
  include Int

  let create =
    let next = ref 0 in
    fun () ->
      Int.incr next;
      !next
  ;;

  let to_alphanum t = [%string "var_%{t#Int}"]
end

module T = struct
  type t =
    | Tok of Token.t
    | Seq of t list
    | Alt of alt
    | Optional of t
    | List of t
    | Sep_list of t * t
    | Annot of t * Type.t
    | Let_rec of
        { bindings : binding list
        ; in_ : t
        }
    | Let of
        { binding : binding
        ; in_ : t
        }
    | Var of Var.t
    | Hierarchy of alt Int.Map.t
    | Enter of t * int

  and tag = string
  and alt = (tag * t) list
  and binding = Var.t * t [@@deriving sexp]
end

include T

let rec pp : t Fmt.t =
 fun ppf t ->
  match t with
  | Tok tok -> Token.pp ppf tok
  | Seq ts -> Fmt.pf ppf "@[%a@]" Fmt.(list ~sep:(any "@;;@;") pp) ts
  | Alt alt -> Fmt.pf ppf "@[(%a)@]" pp_alt alt
  | Var var -> Var.pp ppf var
  | Let_rec { bindings; in_ } ->
    Fmt.pf ppf "@[let rec @[<hv>%a@] in %a@]" pp_bindings bindings pp in_
  | Let { binding; in_ } -> Fmt.pf ppf "@[let %a in %a@]" pp_binding binding pp in_
  | List t -> Fmt.pf ppf "@[{%a}@]" pp t
  | Optional t -> Fmt.pf ppf "@[[%a]@]" pp t
  | Sep_list (t1, t2) -> Fmt.pf ppf "@[{@ %a@ &sep:%a@ }@]" pp t1 pp t2
  | Annot (t, type_) -> Fmt.pf ppf "@[(%a : %a)@]" pp t Type.pp type_
  | Hierarchy levels ->
    Fmt.pf
      ppf
      "@[<hv>hierarchy@.%a@]"
      Fmt.(
        iter_bindings
          ~sep:(any "@.")
          (fun iteri map -> Map.iteri map ~f:(fun ~key ~data -> iteri key data))
          (pair ~sep:(any "@;~@;") int pp_alt))
      levels
  | Enter (t, level) -> Fmt.pf ppf "@[(%a)#%d@]" pp t level

and pp_binding ppf (var, t) = Fmt.pf ppf "@[%a = %a@]" Var.pp var pp t
and pp_bindings ppf bindings = Fmt.(list ~sep:(any "@.") pp_binding) ppf bindings
and pp_alt ppf alt = Fmt.(list ~sep:(any "@;|@;") @@ pair pp_tag pp) ppf alt
and pp_tag = Fmt.string

let tokc tok = Tok (Constant tok)
let tokn name = Tok (Named name)
let tok tok = Tok tok
let alt ts = Alt ts
let seq ts = Seq ts
let annot t ~type_ = Annot (t, type_)

let let_rec rhs ~in_ =
  let var = Var.create () in
  let rhs, in_ =
    let var = Var var in
    rhs var, in_ var
  in
  Let_rec { bindings = [ var, rhs ]; in_ }
;;

let let_ rhs ~in_ =
  let var = Var.create () in
  Let { binding = var, rhs; in_ = in_ (Var var) }
;;

let hierarchy alts =
  let levels = alts |> List.mapi ~f:(fun i alt -> i, alt) |> Int.Map.of_alist_exn in
  Hierarchy levels
;;

let ( .!{} ) t level = Enter (t, level)
let list t = List t
let optional t = Optional t
let sep_list t ~sep = Sep_list (t, sep)

module Structure = struct
  type item =
    | Let of binding
    | Let_rec of binding list
    | Let_tok of
        { name : string
        ; pattern : string
        ; semantics : token_semantics option
        }

  and token_semantics = 
    { type_ : Type.t
    ; lex : (expression [@sexp.opaque])
    ; pp : (expression [@sexp.opaque])
    }


  and t = item list [@@deriving sexp]

  let let_ rhs =
    let var = Var.create () in
    Var var, Let (var, rhs)
  ;;

  let let_rec rhs =
    let var = Var.create () in
    Var var, Let_rec [ var, rhs (Var var) ]
  ;;

  let let_tok ?semantics name pattern = Let_tok { name; pattern; semantics }
end
