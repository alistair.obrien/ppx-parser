(* Generates a menhir specification for the muon grammar *)
open Core
open Ppxlib
open Typing
open (val Ast_builder.make Location.none)

let none t = { txt = t; loc = Location.none }
let transl_type = Expander.Cst.transl_type

let pp_preamble ppf () =
  Fmt.string
    ppf
    {| 
/* Generated parser from Muon */
%{
  open Syntax.Cst
  open Lexing
%}
    |}
;;

let pp_tok ppf tok = Fmt.pf ppf "%s" (Token.to_alphanum tok |> String.capitalize)

let pp_tokens ppf tokens =
  let pp_token ppf (tok, type_) =
    match type_ with
    | Some type_ ->
      Fmt.pf ppf "%%token <%a> %a" Pprintast.core_type (transl_type type_) pp_tok tok
    | None -> Fmt.pf ppf "%%token %a" pp_tok tok
  in
  Fmt.(list ~sep:(any "@.") pp_token) ppf tokens
;;

let pp_entry_points ppf (str : Typed_grammar.Structure.t) =
  let entry_points =
    List.concat_map str ~f:(function
      | Let (var, g) -> [ var, g.type_ ]
      | Let_rec bindings -> List.map bindings ~f:(fun (var, g) -> var, g.type_)
      | Let_tok _ -> [])
  in
  let pp_entry_point ppf (var, type_) =
    Fmt.pf
      ppf
      "%%start <%a> %s"
      Pprintast.core_type
      (transl_type type_)
      (Var.to_alphanum var)
  in
  Fmt.(list ~sep:(any "@.") pp_entry_point) ppf entry_points
;;

module Cfg = struct
  type t = (Var.t * rule) list

  and rule =
    | Alt of alt
    | Hierarchy of alt Int.Map.t
    | Rhs of rhs

  and alt = (string * rhs) list

  and rhs =
    | Sym of symbol
    | Seq of symbol list

  and symbol =
    | Tok of Token.t
    | Var of Var.t
    | Enter of Var.t * int
    | List of symbol
    | Sep_list of symbol * symbol
    | Optional of symbol
  [@@deriving sexp]

  let add_rule cfg rule = cfg := rule :: !cfg
end

let rec compile_rhs ~ctx ~cfg (g : Typed_grammar.t) : Cfg.rhs =
  match g.content with
  | Seq gs -> Seq (List.map ~f:(compile_symbol ~ctx ~cfg) gs)
  | Let { binding; in_ } ->
    let ctx = compile_binding ~ctx ~cfg binding in
    compile_rhs ~ctx ~cfg in_
  | Let_rec { bindings; in_ } ->
    let ctx = compile_rec_bindings ~ctx ~cfg bindings in
    compile_rhs ~ctx ~cfg in_
  | Tok _ | Enter _ | Var _ | Optional _ | List _ | Sep_list _ ->
    Sym (compile_symbol ~ctx ~cfg g)
  | Hierarchy _ | Alt _ -> Sym (Var (compile ~ctx ~cfg g))

and compile_symbol ~ctx ~cfg (g : Typed_grammar.t) : Cfg.symbol =
  match g.content with
  | Tok tok -> Tok tok
  | Enter ({ content = Var var; _ }, level) -> Enter (Map.find_exn ctx var, level)
  | Enter _ -> raise_s [%message "Cannot compile"]
  | Var var -> Var (Map.find_exn ctx var)
  | Optional g -> Optional (compile_symbol ~ctx ~cfg g)
  | List g -> List (compile_symbol ~ctx ~cfg g)
  | Sep_list (g1, g2) ->
    Sep_list (compile_symbol ~ctx ~cfg g1, compile_symbol ~ctx ~cfg g2)
  | _ -> Var (compile ~ctx ~cfg g)

and compile_binding ~ctx ~cfg (var, g) =
  let rule = compile_rule ~ctx ~cfg g in
  let var' = Var.create () in
  Cfg.add_rule cfg (var', rule);
  Map.set ctx ~key:var ~data:var'

and compile_rec_bindings ~ctx ~cfg bindings =
  let ctx =
    List.fold_left bindings ~init:ctx ~f:(fun ctx (var, _) ->
      Map.set ctx ~key:var ~data:(Var.create ()))
  in
  List.iter bindings ~f:(fun (var, g) ->
    let rule = compile_rule ~ctx ~cfg g in
    Cfg.add_rule cfg (Map.find_exn ctx var, rule));
  ctx

and compile ~ctx ~cfg (g : Typed_grammar.t) : Var.t =
  let var = Var.create () in
  Cfg.add_rule cfg (var, compile_rule ~ctx ~cfg g);
  var

and compile_rule ~ctx ~cfg (g : Typed_grammar.t) : Cfg.rule =
  match g.content with
  | Alt alt -> Alt (compile_alt ~ctx ~cfg alt)
  | Hierarchy levels -> Hierarchy (Map.map levels ~f:(compile_alt ~ctx ~cfg))
  | _ -> Rhs (compile_rhs ~ctx ~cfg g)

and compile_alt ~ctx ~cfg (alt : Typed_grammar.alt) : Cfg.alt =
  List.map alt ~f:(fun (tag, g) -> tag, compile_rhs ~ctx ~cfg g)
;;

let compile_str_item ~ctx ~cfg (str_item : Typed_grammar.Structure.item) =
  match str_item with
  | Let (var, g) ->
    Cfg.add_rule cfg (var, compile_rule ~ctx ~cfg g);
    Map.set ctx ~key:var ~data:var
  | Let_rec bindings ->
    List.fold_right bindings ~init:ctx ~f:(fun (var, g) ctx ->
      Cfg.add_rule cfg (var, compile_rule ~ctx ~cfg g);
      Map.set ctx ~key:var ~data:var)
  | Let_tok _ -> ctx
;;

let compile_str str : Cfg.t =
  let ctx = Var.Map.empty in
  let cfg = ref [] in
  let _ctx =
    List.fold_right str ~init:ctx ~f:(fun str_item ctx ->
      compile_str_item ~ctx ~cfg str_item)
  in
  !cfg
;;

let pp_var ppf var = Fmt.pf ppf "%s" (Var.to_alphanum var)
let pp_enter ppf (var, level) = Fmt.pf ppf "%a__at_%d" pp_var var level

let rec pp_sym ppf (sym : Cfg.symbol) =
  match sym with
  | Tok tok -> pp_tok ppf tok
  | Var var -> pp_var ppf var
  | Enter (var, level) -> pp_enter ppf (var, level)
  | List sym -> Fmt.pf ppf "list(%a)" pp_sym sym
  | Sep_list (sym1, sym2) -> Fmt.pf ppf "sep_list(%a, %a)" pp_sym sym1 pp_sym sym2
  | Optional sym -> Fmt.pf ppf "option(%a)" pp_sym sym
;;

let pp_rhs ppf (rhs : Cfg.rhs) =
  match rhs with
  | Sym sym ->
    Fmt.pf ppf "x = %a" pp_sym sym;
    pexp_ident (none (Lident "x"))
  | Seq syms ->
    let syms = List.mapi syms ~f:(fun i sym -> i, sym) in
    let pp_sym ppf (i, sym) = Fmt.pf ppf "x%d = %a" i pp_sym sym in
    let tuple =
      pexp_tuple
        (List.map syms ~f:(fun (i, _) -> pexp_ident (none (Lident [%string "x%{i#Int}"]))))
    in
    Fmt.pf ppf "%a" Fmt.(list ~sep:(any "; ") pp_sym) syms;
    tuple
;;

let pp_alt ppf (alt : Cfg.alt) =
  (* Nasty use of references, but best way I could think of doing it... *)
  let action = ref None in
  let pp_rhs ppf rhs = action := Some (pp_rhs ppf rhs) in
  let pp_action ppf () = Pprintast.expression ppf (Option.value_exn !action) in
  let pp_alt ppf (tag, rhs) =
    let tag = String.capitalize tag in
    Fmt.pf ppf "%a { %s (%a) }" pp_rhs rhs tag pp_action ()
  in
  Fmt.pf ppf "@[<v> | %a@]" Fmt.(list ~sep:(any "@. | ") pp_alt) alt
;;

let pp_rhs ppf rhs =
  let action = ref None in
  let pp_rhs ppf rhs = action := Some (pp_rhs ppf rhs) in
  let pp_action ppf () = Pprintast.expression ppf (Option.value_exn !action) in
  Fmt.pf ppf "%a { %a }" pp_rhs rhs pp_action ()
;;

let pp_rule ppf (var, (rule : Cfg.rule)) =
  match rule with
  | Alt alt -> Fmt.pf ppf "%a:@.%a" pp_var var pp_alt alt
  | Hierarchy levels ->
    let levels = Map.to_alist levels in
    List.iter levels ~f:(fun (level, alt) ->
      Fmt.pf ppf "%a:@.%a@. ;@." pp_enter (var, level) pp_alt alt)
  | Rhs rhs -> Fmt.pf ppf "%a:@. | %a@. ;" pp_var var pp_rhs rhs
;;

let pp_rules ppf rules = Fmt.(list ~sep:(any "@.@.") pp_rule) ppf rules

let expand ppf (str : Grammar.Structure.t) =
  let ctx = Ctx.empty () in
  let _ctx, str = infer_str ~ctx str in
  let tokens =
    let named_tokens =
      List.map (Expander.Token_.named_toks str) ~f:(fun (tok, semantics) ->
        Token.Named tok, Option.map ~f:(fun semantics -> semantics.type_) semantics)
    in
    let const_tokens =
      Expander.Token_.const_toks_str str
      |> Set.to_list
      |> List.map ~f:(fun const -> Token.Constant const, None)
    in
    named_tokens @ const_tokens
  in
  let rules = compile_str str in
  Fmt.pf
    ppf
    "%a@.%a@.%a@.%%%%@.%a"
    pp_preamble
    ()
    pp_tokens
    tokens
    pp_entry_points
    str
    pp_rules
    rules
;;
