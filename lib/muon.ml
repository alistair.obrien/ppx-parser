open Ppxlib
module Token = Token
module Type = Type

module Grammar : sig
  type t [@@deriving sexp]

  val pp : t Fmt.t
  val tokc : string -> t
  val tokn : string -> t
  val tok : Token.t -> t
  val alt : (string * t) list -> t
  val seq : t list -> t
  val let_rec : (t -> t) -> in_:(t -> t) -> t
  val let_ : t -> in_:(t -> t) -> t
  val hierarchy : (string * t) list list -> t
  val ( .!{} ) : t -> int -> t
  val annot : t -> type_:Type.t -> t
  val list : t -> t
  val optional : t -> t
  val sep_list : t -> sep:t -> t

  module Structure : sig
    type g := t

    type t = item list
    and item [@@deriving sexp]

    and token_semantics =
      { type_ : Type.t
      ; lex : (expression[@sexp.opaque])
      ; pp : (expression[@sexp.opaque])
      }

    val let_ : g -> g * item
    val let_rec : (g -> g) -> g * item
    val let_tok : ?semantics:token_semantics -> string -> string -> item
  end

  val expand : Structure.t -> Ppxlib.structure_item list
  val pr_expansion : Structure.t -> unit
  val pr_menhir_spec : Structure.t -> unit
end = struct
  include Grammar

  let expand = Expander.expand

  let pr_expansion str =
    let expansion = expand str in
    Fmt.(list ~sep:(any "@.") Pprintast.structure_item) Fmt.stdout expansion
  ;;

  let pr_menhir_spec str = Menhir.expand Fmt.stdout str
end
