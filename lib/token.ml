open Core

module T = struct
  type t =
    | Constant of string
        (** A constant string defining a token such as ["("] or ["fun"]. Often 
            consisting of symbols or keywords. 

            The type name associated with tokens of this form consist of 
            the alphanumeric encoding of the token (See [to_alphanum])
        *)
    | Named of string
        (** A user-defined token written as [@token_name]. 
            
            The type name associated with tokens of this form 
            are the token name [token_name].
        *)
  [@@deriving equal, compare, sexp]
end

include T

let pp ppf t =
  match t with
  | Constant const -> Fmt.pf ppf "\"%s\"" const
  | Named name -> Fmt.pf ppf "@%s" name
;;

module To_alphanum = struct
  (* Mappings for symbols to identifiers commonly used in programming languages *)

  (* Fallback for bytes outside the ascii range. *)
  let unknown = "unk"

  let table =
    Char.Map.of_alist_exn
      [ '\x00', "nul"
      ; '\x01', "soh"
      ; '\x02', "stx"
      ; '\x03', "eta"
      ; '\x04', "eot"
      ; '\x05', "enq"
      ; '\x06', "ack"
      ; '\x07', "bel"
      ; '\x08', "bs"
      ; '\x09', "ht"
      ; '\x0A', "lf"
      ; '\x0B', "vt"
      ; '\x0C', "ff"
      ; '\x0D', "cr"
      ; '\x0E', "so"
      ; '\x0F', "si"
      ; '\x10', "dle"
      ; '\x11', "dc1"
      ; '\x12', "dc2"
      ; '\x13', "dc3"
      ; '\x14', "dc4"
      ; '\x15', "nak"
      ; '\x16', "syn"
      ; '\x17', "etb"
      ; '\x18', "can"
      ; '\x19', "em"
      ; '\x1A', "sub"
      ; '\x1B', "esc"
      ; '\x1C', "fs"
      ; '\x1D', "gs"
      ; '\x1E', "rs"
      ; '\x1F', "us"
      ; ' ', "space"
      ; '!', "bang"
      ; '"', "dquot"
      ; '#', "hash"
      ; '$', "dollar"
      ; '%', "perc"
      ; '&', "amp"
      ; '\'', "squot"
      ; '(', "lpar"
      ; ')', "rpar"
      ; '*', "star"
      ; '+', "plus"
      ; ',', "comma"
      ; '-', "dash"
      ; '.', "dot"
      ; '/', "slash"
      ; (* omit 0-9 *)
        ':', "colon"
      ; ';', "semi"
      ; '<', "lt"
      ; '=', "eq"
      ; '>', "gt"
      ; '?', "qmark"
      ; '@', "at"
      ; (* omit A-Z *)
        '[', "lbrack"
      ; '\\', "bslash"
      ; ']', "rbrack"
      ; '^', "hat"
      ; (* omit _ *)
        '`', "bquot"
      ; (* omit a-z *)
        '{', "lcurl"
      ; '|', "bar"
      ; '}', "rcurl"
      ; '~', "tilde"
      ; '\x7F', "del"
      ]
  ;;

  let char_to_alphanum c = if Char.to_int c < 128 then Map.find table c else Some unknown

  let to_alphanum s =
    let buf = Buffer.create 100 in
    String.iter
      ~f:(fun c ->
        match char_to_alphanum c with
        | None -> Buffer.add_char buf c
        | Some name ->
          if Buffer.length buf <> 0 then Buffer.add_char buf '_';
          Buffer.add_string buf name)
      s;
    Buffer.contents buf
  ;;
end

let to_alphanum t =
  match t with
  | Constant const -> To_alphanum.to_alphanum const
  | Named name -> name
;;

include Comparable.Make (T)