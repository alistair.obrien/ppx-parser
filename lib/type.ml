open Core
module Constr : Identifiable.S with type t = string = String

(* Encoding of the types of grammars. Contains a subset of OCaml's types
   with hierarchy and token types. 
*)
type t =
  | Unit (** The unit type [unit] *)
  | Tok of Token.t
      (** A token type. A token [tok] will have the token type [Token tok].
        
        This typing rule is useful for generating the [Cst] types. For example, 
        the grammar
        
          seq [tok "("; expr; tok ")"]
        
        Has the type [Tuple [Tok (Constant "("); expr; Tok (Constant ")")]], which 
        is expanded to the OCaml type:

          Token.t_lpar * expr * Token.t_rpar
    *)
  | Constr of
      { constr : Constr.t
      ; params : t list
      }
      (** The constructor type [(params) constr] from OCaml. 
      
          Used to encode ['a list], ['a option] and nullary user-defined 
          constructors [expr] in Muon.
      *)
  | Tuple of t list (** The tuple type [t1 * ... * tn] *)
  | Hierarchy of t * Int.Set.t
      (** The hierarchy type [t hierarchy{dom}] denotes a hierarchy with
          return type [t] and level domain [dom].
      
          Used in type checking to ensure level's are correctly called into. 
      *)
[@@deriving equal, compare, sexp]

let unit = Unit
let tok tok = Tok tok
let constr0 constr = Constr { constr; params = [] }
let constr1 constr param = Constr { constr; params = [ param ] }
let constr constr params = Constr { constr; params }
let tuple ts = Tuple ts
let hierarchy t n = Hierarchy (t, List.range ~stop:`exclusive 0 n |> Int.Set.of_list)
let option t = Constr { constr = "option"; params = [ t ] }
let list t = Constr { constr = "list"; params = [ t ] }
let sep_list t1 t2 = Constr { constr = "sep_list"; params = [ t1; t2 ] }

let rec pp ppf t =
  match t with
  | Unit -> Fmt.pf ppf "unit"
  | Tok tok -> Token.pp ppf tok
  | Constr { constr; params } ->
    Fmt.pf ppf "@[(%a) %s@]" Fmt.(list ~sep:(any ",@ ") pp) params constr
  | Tuple ts -> Fmt.(list ~sep:(any " * ") pp) ppf ts
  | Hierarchy (t, dom) ->
    Fmt.pf
      ppf
      "@[%a hierarchy{%a}@]"
      pp
      t
      Fmt.(iter ~sep:(any ",@ ") (fun f dom -> Set.iter ~f dom) int)
      dom
;;

module Tag : Identifiable.S with type t = string = String

type decl =
  { constr : Constr.t
  ; kind : kind
  }

and kind =
  | Alias of t
  | Variant of t Tag.Map.t
[@@deriving equal, compare, sexp]
