open Core
module Var = Grammar.Var

module Typed_grammar = struct
  type t =
    { content : content
    ; type_ : Type.t
    }

  and content =
    | Tok of Token.t
    | Seq of t list
    | Alt of alt
    | Optional of t
    | List of t
    | Sep_list of t * t
    | Let_rec of
        { bindings : binding list
        ; in_ : t
        }
    | Let of
        { binding : binding
        ; in_ : t
        }
    | Var of Var.t
    | Hierarchy of alt Int.Map.t
    | Enter of t * int

  and tag = string
  and binding = Var.t * t
  and alt = (tag * t) list [@@deriving sexp]

  let type_ { type_; _ } = type_

  module Structure = struct
    type item =
      | Let of binding
      | Let_rec of binding list
      | Let_tok of
          { name : string
          ; pattern : string
          ; semantics : Grammar.Structure.token_semantics option
          }

    and t = item list [@@deriving sexp]
  end
end

module I = Grammar
module O = Typed_grammar

module Ctx = struct
  type t =
    { bindings : Type.t Var.Map.t
    ; tokens : Type.t option String.Map.t
    ; type_decls : (Type.Constr.t, Type.decl) Hashtbl.t
    }

  let type_decls { type_decls; _ } =
    type_decls |> Hashtbl.to_alist |> Type.Constr.Map.of_alist_exn
  ;;

  let tokens { tokens; _ } = tokens

  let empty () =
    { bindings = Var.Map.empty
    ; tokens = String.Map.empty
    ; type_decls = Hashtbl.create (module Type.Constr)
    }
  ;;

  let add t var type_ = { t with bindings = Map.set t.bindings ~key:var ~data:type_ }
  let add_tok t name type_ = { t with tokens = Map.set t.tokens ~key:name ~data:type_ }
  let find t var = Map.find t.bindings var
  let find_tok t name = Map.find t.tokens name
  let mem_tok t name = Map.mem t.tokens name

  let merge_kind (kind1 : Type.kind) (kind2 : Type.kind) : Type.kind =
    let fail () =
      raise_s
        [%message
          "Incompatiable declarations of the same type"
            (kind1 : Type.kind)
            (kind2 : Type.kind)]
    in
    match kind1, kind2 with
    | Alias t1, Alias t2 ->
      if Type.compare t1 t2 <> 0 then fail ();
      Alias t1
    | Variant tags1, Variant tags2 ->
      let tags =
        Map.merge_skewed tags1 tags2 ~combine:(fun ~key:_ t1 t2 ->
          if Type.compare t1 t2 <> 0 then fail ();
          t1)
      in
      Variant tags
    | _, _ -> fail ()
  ;;

  let add_decl t constr kind =
    Hashtbl.update t.type_decls constr ~f:(function
      | None -> { constr; kind }
      | Some { kind = kind'; _ } -> { constr; kind = merge_kind kind kind' })
  ;;

  let rec unify t type1 type2 =
    let open Type in
    match type1, type2 with
    | Unit, Unit -> ()
    | Tuple types1, Tuple types2 -> List.iter2_exn ~f:(unify t) types1 types2
    | Tok tok1, Tok tok2 when Token.(tok1 = tok2) -> ()
    | Constr { constr = c1; params = p1 }, Constr { constr = c2; params = p2 }
      when Constr.(c1 = c2) -> List.iter2_exn ~f:(unify t) p1 p2
    | Constr { constr; params = [] }, type_ | type_, Constr { constr; params = [] } ->
      add_decl t constr (Alias type_)
    | Hierarchy (type1, dom1), Hierarchy (type2, dom2) when Set.equal dom1 dom2 ->
      unify t type1 type2
    | _, _ -> raise_s [%message "Cannot unify types" (type1 : Type.t) (type2 : Type.t)]
  ;;
end

let rec infer ~ctx (t : I.t) : O.t =
  let open Type in
  let infer ?(ctx = ctx) t = infer ~ctx t in
  let check ?(ctx = ctx) t type_ = check ~ctx t type_ in
  let return content type_ : O.t = { content; type_ } in
  match t with
  | Tok (Constant _ as tok) -> return (Tok tok) (Tok tok)
  | Tok (Named name as tok) ->
    (* Check [name] is in scope *)
    if not @@ Ctx.mem_tok ctx name
    then raise_s [%message "Undefined token" (tok : Token.t)];
    return (Tok tok) (Tok tok)
  | Seq ts ->
    let ts = List.map ~f:infer ts in
    let tuple = Tuple (List.map ~f:O.type_ ts) in
    return (Seq ts) tuple
  | Optional t ->
    let t = infer t in
    return (Optional t) (option t.type_)
  | List t ->
    let t = infer t in
    return (List t) (list t.type_)
  | Sep_list (t1, t2) ->
    let t1 = infer t1 in
    let t2 = infer t2 in
    return (Sep_list (t1, t2)) (sep_list t1.type_ t2.type_)
  | Var var ->
    (match Ctx.find ctx var with
     | Some type_ -> return (Var var) type_
     | None -> raise_s [%message "Unbound variable" (var : Var.t)])
  | Let { binding; in_ } ->
    let ctx, binding = infer_binding ~ctx binding in
    let in_ = infer ~ctx in_ in
    return (Let { binding; in_ }) in_.type_
  | Let_rec { bindings; in_ } ->
    let ctx, bindings = infer_rec_bindings ~ctx bindings in
    let in_ = infer ~ctx in_ in
    return (Let_rec { bindings; in_ }) in_.type_
  | Enter (t, level) ->
    let t' = infer t in
    (match t'.type_ with
     | Hierarchy (type_, level_set) ->
       if not @@ Set.mem level_set level
       then raise_s [%message "Unbound level in hierarchy"];
       return (Enter (t', level)) type_
     | _ -> raise_s [%message "Expected hierarchy" (t : I.t)])
  | Annot (t, constr) ->
    let t = check ~ctx t constr in
    t
  | Hierarchy _ | Alt _ -> raise_s [%message "Cannot infer hierarchy or alt"]

and check ~ctx (t : I.t) (type_ : Type.t) : O.t =
  let check ?(ctx = ctx) t type_ = check ~ctx t type_ in
  let infer ?(ctx = ctx) t = infer ~ctx t in
  let return content : O.t = { content; type_ } in
  let lift (t : O.t) : O.t = return t.content in
  match t, type_ with
  | Alt alt, Constr { constr; params = [] } ->
    let variants = check_alt ~ctx alt constr in
    return (Alt variants)
  | Hierarchy levels, Hierarchy (Constr { constr; params = [] }, dom) ->
    if not @@ Set.equal dom (Map.key_set levels)
    then raise_s [%message "Inconsistent levels"];
    let levels = Map.map levels ~f:(fun t -> check_alt ~ctx t constr) in
    return (Hierarchy levels)
  | Let { binding; in_ }, _ ->
    let ctx, binding = infer_binding ~ctx binding in
    let in_ = check ~ctx in_ type_ in
    return (Let { binding; in_ })
  | Let_rec { bindings; in_ }, _ ->
    let ctx, bindings = infer_rec_bindings ~ctx bindings in
    let in_ = check ~ctx in_ type_ in
    return (Let_rec { bindings; in_ })
  | t, _ ->
    let t = infer t in
    Ctx.unify ctx type_ t.type_;
    lift t

and check_alt ~ctx (alt : I.alt) (type_ : Type.Constr.t) : O.alt =
  let variants = List.map alt ~f:(fun (tag, t) -> tag, infer ~ctx t) in
  let tags =
    variants |> List.map ~f:(fun (tag, t) -> tag, t.type_) |> Type.Tag.Map.of_alist_exn
  in
  Ctx.add_decl ctx type_ (Variant tags);
  variants

and infer_binding ~ctx (var, t) =
  let t = infer ~ctx t in
  Ctx.add ctx var t.type_, (var, t)

and infer_rec_bindings ~ctx rec_bindings =
  let rec_bindings =
    List.map rec_bindings ~f:(fun (var, t) ->
      match t with
      | Annot (t, constr) -> var, constr, t
      | _ -> raise_s [%message "Recursive let-bindings must be annotated"])
  in
  let ctx =
    List.fold rec_bindings ~init:ctx ~f:(fun ctx (var, type_, _) -> Ctx.add ctx var type_)
  in
  let rec_bindings =
    List.map rec_bindings ~f:(fun (var, constr, t) ->
      let t = check ~ctx t constr in
      var, t)
  in
  ctx, rec_bindings

and infer_str_item ~ctx (item : I.Structure.item) : Ctx.t * O.Structure.item =
  match item with
  | Let binding ->
    let ctx, binding = infer_binding ~ctx binding in
    ctx, Let binding
  | Let_rec bindings ->
    let ctx, bindings = infer_rec_bindings ~ctx bindings in
    ctx, Let_rec bindings
  | Let_tok { name; pattern; semantics = Some semantics } ->
    ( Ctx.add_tok ctx name (Some semantics.type_)
    , Let_tok { name; pattern; semantics = Some semantics } )
  | Let_tok { name; pattern; semantics = None } ->
    Ctx.add_tok ctx name None, Let_tok { name; pattern; semantics = None }

and infer_str ~ctx str =
  List.fold_map str ~init:ctx ~f:(fun ctx str_item -> infer_str_item ~ctx str_item)
;;
