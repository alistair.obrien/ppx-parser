open Core

module Var : sig
  type t

  val create : unit -> t

  include Identifiable.S with type t := t
end = struct
  include Int

  let create =
    let next = ref 0 in
    fun () ->
      Int.incr next;
      !next
  ;;
end

module T = struct
  type ('tok, 'a) t =
    { content : ('tok, 'a) content
    ; data : 'a
    }

  (* Rename [tok] to [label] or smth *)
  and ('tok, 'a) content =
    | Fail
    | Empty
    | Tok of Token.t * 'tok
    | Seq of ('tok, 'a) t * ('tok, 'a) t
    | Alt of ('tok, 'a) t * ('tok, 'a) t
    | Rep of ('tok, 'a) t
    | Var of Var.t * 'tok
    | Let_rec of Var.t * ('tok, 'a) t * ('tok, 'a) t
  [@@deriving sexp, hash, compare]
end

include T

let create content = { content; data = () }
let fail = create Fail
let empty = create Empty
let tok tok = create (Tok (tok, ()))
let seq t1 t2 = create (Seq (t1, t2))
let alt t1 t2 = create (Alt (t1, t2))
let rep t = create (Rep t)

let let_rec f ~in_ =
  let var = Var.create () in
  let var_t = create (Var (var, ())) in
  create (Let_rec (var, f var_t, in_ var_t))
;;
