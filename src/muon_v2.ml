module Grammar = Grammar

(* module Parser = Parser *)
module Token = Token
module State_machine = State_machine
module Stack_machine = Stack_machine

let expr =
  let open Grammar in
  let a = tok @@ Token.create 'a' in
  let b = tok @@ Token.create 'b' in
  alt (rep (seq a (rep (seq a b)))) (rep (seq a b))
;;

let g =
  let open Grammar in
  let a = tok @@ Token.create 'a' in
  let b = tok @@ Token.create 'b' in
  let_rec (fun s -> alt a (seq s (seq b s))) ~in_:(fun s -> s)
;;

let input =
  let a = Token.create 'a' in
  let b = Token.create 'b' in
  Array.of_list [ a; b; a ] |> Stack_machine.Input.create
;;
