(* open Core
open Grammar

module Annot = struct
  module Symbol = struct
    module T = struct
      type t =
        | Tok of Token.t
        | Call of Rule_call.t
        | Accept
      [@@deriving compare, sexp]
    end

    include T
    include Comparable.Make (T)

    let call t = 
      match t with
      | Call call -> Some call
      | Tok _ | Accept -> None
  end

  module Pair = struct
    module T = struct
      type t = Symbol.t * Symbol.t [@@deriving sexp, compare]
    end

    include T
    include Comparable.Make (T)
  end

  module T = struct
    type t =
      { null : bool
      ; first : Symbol.Set.t
      ; last : Symbol.Set.t
      ; pair_set : Pair.Set.t
      }
    [@@deriving compare, sexp]
  end

  include T

  let pp ppf t = sexp_of_t t |> Sexp.pp_hum ppf
  let ( => ) b syms = if b then syms else Symbol.Set.empty

  let fail =
    { null = false
    ; last = Symbol.Set.empty
    ; first = Symbol.Set.empty
    ; pair_set = Pair.Set.empty
    }
  ;;

  let empty =
    { null = true
    ; last = Symbol.Set.empty
    ; first = Symbol.Set.empty
    ; pair_set = Pair.Set.empty
    }
  ;;

  let tok tok =
    { null = false
    ; first = Symbol.Set.singleton (Tok tok)
    ; last = Symbol.Set.singleton (Tok tok)
    ; pair_set = Pair.Set.empty
    }
  ;;

  let alt t1 t2 =
    { null = t1.null || t2.null
    ; first = Set.union t1.first t2.first
    ; last = Set.union t1.last t2.last
    ; pair_set = Set.union t1.pair_set t2.pair_set
    }
  ;;

  let seq t1 t2 =
    { null = t1.null && t2.null
    ; first = Set.union t1.first (t1.null => t2.first)
    ; last = Set.union t2.last (t2.null => t1.last)
    ; pair_set =
        (let pair_set = Set.union t1.pair_set t2.pair_set in
         Set.union pair_set
         @@ Pair.Set.of_list
              (List.cartesian_product (Set.to_list t1.last) (Set.to_list t2.first)))
    }
  ;;

  let fix f =
    let rec loop t =
      let t' = f t in
      if compare t t' = 0 then t else loop t'
    in
    loop fail
  ;;

  (* include Comparable.Make (T) *)
  let equal t1 t2 = compare t1 t2 = 0
end

let fix_ctx f =
  let rec loop ctx =
    let ctx' = f ctx in
    if Map.equal Annot.equal ctx ctx' then ctx else loop ctx'
  in
  loop Var.Map.empty
;;


let fix_map ~init ~equal f =
  let rec loop map =
    let map' = f map in
    if Map.equal equal map map' then map else loop map'
  in
  loop init
;;

let fix_set ~init f =
  let rec loop set =
    let set' = f set in
    if Set.equal set set' then set else loop set'
  in
  loop init
;;

let rec annotate_rules rules =
  fix_ctx (fun ctx ->
    Map.fold rules ~init:ctx ~f:(fun ~key:var ~data:expr ctx ->
      let annot = annotate_expr ~ctx expr in
      Map.set ctx ~key:var ~data:annot))

and annotate_expr ~ctx (expr : Expr.t) =
  let open Annot in
  let self ?(ctx = ctx) expr = annotate_expr ~ctx expr in
  match expr with
  | Fail -> fail
  | Empty -> empty
  | Tok tok' -> tok tok'
  | Seq (e1, e2) -> seq (self e1) (self e2)
  | Alt (e1, e2) -> alt (self e1) (self e2)
  | Call call ->
    (match Map.find ctx (Rule_call.rule call) with
     | Some annot -> annot
     | None -> fail)
;;

let annotate { rules; start } =
  let ctx = annotate_rules rules in
  ctx, annotate_expr ~ctx start
;;

module Context = struct
  module rec T : sig
    type t = { origins : T_comparable.Set.t Rule_call.Map.t } [@@deriving sexp, compare]
  end =
    T

  and T_comparable : (Comparable.S with type t := T.t) = Comparable.Make (T)

  include T
  include T_comparable
end

module Frame = struct
  module T = struct
    type t = Rule_call.t * Context.t [@@deriving sexp, compare]
  end

  include T
  include Comparable.Make (T)
end


let call_set ~ctx rule_call = 
  fix_map ~init:Rule_call.Map.empty (fun call_set_map ->
    let annot : Annot.t = Map.find_exn ctx (Rule_call.rule rule_call) in
    let first_calls = annot.first |> Set.filter_map (module Rule_call) ~f:Annot.Symbol.call in
    let last_calls = annot.last |> Set.filter_map (module Rule_call) ~f:Annot.Symbol.call in
    (* Add calls in [first_calls] to [call_set] *)
    let call_set_map = Set.fold first_calls ~init:call_set_map ~f:(fun call_set_map call ->
      let call_set = Option.value (Map.find call_set_map call) ~default:(Frame.Set.empty) in


    )
    in
    assert false
    )

module State = struct
  type t = { transitions : Symbol.Set.t Symbol.Map.t }
end *)
