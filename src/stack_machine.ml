open Core
open Grammar
open State_machine

module Pos : sig
  type t = private int [@@deriving sexp, compare, hash]

  val start : t
  val next : t -> t
end = struct
  type t = int [@@deriving sexp, compare, hash]

  let start = 0
  let next t = t + 1
end

module Input : sig
  type t [@@deriving sexp]

  val get : t -> Pos.t -> Token.t
  val create : Token.t Array.t -> t
  val eof : t -> Pos.t -> bool
  val out_of_bounds : t -> Pos.t -> bool
end = struct
  type t = Token.t Array.t [@@deriving sexp]

  let create arr : t = Array.copy arr
  let get t (pos : Pos.t) = t.((pos :> int))
  let eof t (pos : Pos.t) = Array.length t = (pos :> int) + 1
  let out_of_bounds t (pos : Pos.t) = Array.length t <= (pos :> int)
end

module Ctx = struct
  module Ident = struct
    module T = struct
      type t = Var.t * Pos.t [@@deriving sexp, hash, compare]
    end

    include T
    include Comparable.Make (T)
  end

  module Origin = struct
    module T = struct
      type t =
        { return_state : State.t
        ; caller_ctx : Ident.t
        }
      [@@deriving sexp, hash, compare]
    end

    include T
    include Comparable.Make (T)
  end

  type t = Origin.t Hash_set.t

  let sexp_of_t t = t |> Hash_set.to_list |> List.sexp_of_t Origin.sexp_of_t
  let create () = Hash_set.create (module Origin)
  let add_origin t origin = Hash_set.add t origin
end

module Stack : sig
  type t [@@deriving sexp_of]

  val create : unit -> t
  val add : t -> ident:Ctx.Ident.t -> ctx:Ctx.t -> unit
  val find : t -> Ctx.Ident.t -> Ctx.t option
  val find_or_add : t -> Ctx.Ident.t -> Ctx.t
end = struct
  type t = (Ctx.Ident.t, Ctx.t) Hashtbl.t

  let sexp_of_t t =
    t |> Hashtbl.to_alist |> List.sexp_of_t [%sexp_of: Ctx.Ident.t * Ctx.t]
  ;;

  let create () = Hashtbl.create (module Ctx.Ident)
  let add t ~ident ~ctx = Hashtbl.set t ~key:ident ~data:ctx
  let find = Hashtbl.find
  let find_or_add t key = Hashtbl.find_or_add t key ~default:(fun () -> Ctx.create ())
end

module Frames = struct
  type t = State.Set.t Ctx.Ident.Map.t [@@deriving sexp]

  let create init_state =
    let ident = Var.create (), Pos.start in
    Ctx.Ident.Map.singleton ident (State.Set.singleton init_state)
  ;;
end

let parse grammar input =
  Fmt.pr
    "Grammar:@. %a@."
    Sexp.pp_hum
    (Grammar.sexp_of_t Unit.sexp_of_t Unit.sexp_of_t grammar);
  let state_machine = State_machine.create grammar in
  Fmt.pr "State Machine:@. %a@." Sexp.pp_hum (State_machine.sexp_of_t state_machine);
  Fmt.pr "State Machine Dot:@. %s@." (State_machine.to_dot state_machine);
  let init_frames = Frames.create state_machine.init_state in
  let stack = Stack.create () in
  (* frames, pos -> [> `Next (frames, pos) | `Error | `Accept ] *)
  let step (frames : Frames.t) pos =
    if Input.out_of_bounds input pos
    then (
      Fmt.pr
        "Error: Processing frames out of bounds@.Frames: %a@."
        Sexp.pp_hum
        (Frames.sexp_of_t frames);
      `Error)
    else (
      let tok = Input.get input pos in
      let next_frames = Hashtbl.create (module Ctx.Ident) in
      let accepted = ref false in
      (* call : Ctx.t -> State.t -> unit *)
      let rec call caller_ctx return_state var =
        let ctx = var, pos in
        match Stack.find stack (var, pos) with
        | Some rule_ctx -> Ctx.add_origin rule_ctx { caller_ctx; return_state }
        | None ->
          let rule_ctx = Ctx.create () in
          Ctx.add_origin rule_ctx { caller_ctx; return_state };
          Stack.add stack ~ident:ctx ~ctx:rule_ctx;
          let init_state = Map.find_exn state_machine.rules var in
          process_state ctx init_state
      (* process_state : Ctx.t -> State.t -> State.Set.t *)
      and process_state ctx curr_state =
        let next_states = Hash_set.create (module State) in
        Map.find_exn state_machine.actions curr_state
        |> Set.iter ~f:(function
             | Shift (Tok tok', next_state) ->
               if Token.(tok = tok') then Hash_set.add next_states next_state
             | Shift (Var var, return_state) -> call ctx return_state var
             | Reduce _var ->
               (* Assume no ambiguous reduces for now *)
               Stack.find stack ctx
               |> Option.value_exn
               |> Hash_set.iter ~f:(fun { caller_ctx; return_state } ->
                    process_state caller_ctx return_state)
             | Accept -> accepted := true);
        let next_states = next_states |> Hash_set.to_list |> State.Set.of_list in
        if not (Set.is_empty next_states)
        then
          Hashtbl.update next_frames ctx ~f:(function
            | Some next_states' -> Set.union next_states' next_states
            | None -> next_states)
      and process ctx active_states = active_states |> Set.iter ~f:(process_state ctx) in
      Map.iteri frames ~f:(fun ~key:ctx ~data:active_states -> process ctx active_states);
      let next_frames = Map.of_hashtbl_exn (module Ctx.Ident) next_frames in
      if !accepted && Input.eof input pos
      then `Accept
      else if Map.is_empty next_frames
      then `Error
      else `Next (next_frames, Pos.next pos))
  in
  let rec driver frames pos =
    Fmt.pr "Stack: %a@." Sexp.pp_hum (Stack.sexp_of_t stack);
    Fmt.pr "Frames: %a@." Sexp.pp_hum (Frames.sexp_of_t frames);
    match step frames pos with
    | `Accept -> `Accept
    | `Error -> `Error
    | `Next (frames, pos) -> driver frames pos
  in
  driver init_frames Pos.start
;;
