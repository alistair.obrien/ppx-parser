open Core
open Grammar
module Label = Token.Label
module State = Label

module Symbol = struct
  module T = struct
    type t =
      | Tok of Token.t
      | Var of Var.t
    [@@deriving sexp, compare, hash]
  end

  include T
  include Comparable.Make (T)
end

module Labelled (T : sig
  type t [@@deriving sexp, compare]

  include Comparable.S with type t := t
end) : sig
  type t = T.t * Label.t [@@deriving sexp, compare]

  include Comparable.S with type t := t
end = struct
  module T = struct
    type t = T.t * Label.t [@@deriving sexp, compare]
  end

  include T
  include Comparable.Make (T)
end

module Labelled_symbol = Labelled (Symbol)

module Action = struct
  module T = struct
    type t =
      | Shift of Symbol.t * State.t
      | Reduce of Var.t
      | Accept
    [@@deriving sexp, compare, hash]
  end

  include T
  include Comparable.Make (T)
end

let rec label g =
  let return content = { content; data = g.data } in
  match g.content with
  | Fail -> return Fail
  | Empty -> return Empty
  | Tok (tok, _) -> return @@ Tok (tok, Label.create ())
  | Seq (t1, t2) -> return @@ Seq (label t1, label t2)
  | Alt (t1, t2) -> return @@ Alt (label t1, label t2)
  | Rep t -> return @@ Rep (label t)
  | Var (var, _) -> return @@ Var (var, Label.create ())
  | Let_rec (var, rhs, in_) -> return @@ Let_rec (var, label rhs, label in_)
;;

module Annot = struct
  module Symbol = Labelled_symbol

  module Pair = struct
    module T = struct
      type t = Symbol.t * Symbol.t [@@deriving sexp, compare]
    end

    include T
    include Comparable.Make (T)
  end

  module T = struct
    type t =
      { null : bool
      ; first : Symbol.Set.t
      ; last : Symbol.Set.t
      ; pair_set : Pair.Set.t
      }
    [@@deriving compare, sexp]
  end

  include T

  let ( => ) b syms = if b then syms else Symbol.Set.empty

  let fail =
    { null = false
    ; last = Symbol.Set.empty
    ; first = Symbol.Set.empty
    ; pair_set = Pair.Set.empty
    }
  ;;

  let empty =
    { null = true
    ; last = Symbol.Set.empty
    ; first = Symbol.Set.empty
    ; pair_set = Pair.Set.empty
    }
  ;;

  let tok (tok, label) =
    { null = false
    ; first = Symbol.Set.singleton (Tok tok, label)
    ; last = Symbol.Set.singleton (Tok tok, label)
    ; pair_set = Pair.Set.empty
    }
  ;;

  let alt t1 t2 =
    { null = t1.null || t2.null
    ; first = Set.union t1.first t2.first
    ; last = Set.union t1.last t2.last
    ; pair_set = Set.union t1.pair_set t2.pair_set
    }
  ;;

  let seq t1 t2 =
    { null = t1.null && t2.null
    ; first = Set.union t1.first (t1.null => t2.first)
    ; last = Set.union t2.last (t2.null => t1.last)
    ; pair_set =
        (let pair_set = Set.union t1.pair_set t2.pair_set in
         Set.union pair_set
         @@ Pair.Set.of_list
              (List.cartesian_product (Set.to_list t1.last) (Set.to_list t2.first)))
    }
  ;;

  let fix f =
    let rec loop t =
      let t' = f t in
      if compare t t' = 0 then t else loop t'
    in
    loop fail
  ;;

  let rep t = fix (fun type_ -> alt empty (seq t type_))

  let var t (var, label) =
    { null = t.null
    ; first = Symbol.Set.singleton (Var var, label)
    ; last = Symbol.Set.singleton (Var var, label)
    ; pair_set = Pair.Set.empty
    }
  ;;
end

let rec annotate ~ctx g =
  let self ?(ctx = ctx) g = annotate ~ctx g in
  let return content annot = { content; data = annot } in
  match g.content with
  | Empty -> return Empty Annot.empty
  | Fail -> return Fail Annot.fail
  | Tok (tok, l) -> return (Tok (tok, l)) @@ Annot.tok (tok, l)
  | Alt (g1, g2) ->
    let g1 = self g1
    and g2 = self g2 in
    return (Alt (g1, g2)) @@ Annot.alt g1.data g2.data
  | Seq (g1, g2) ->
    let g1 = self g1
    and g2 = self g2 in
    return (Seq (g1, g2)) @@ Annot.seq g1.data g2.data
  | Rep g ->
    let g = self g in
    return (Rep g) @@ Annot.rep g.data
  | Var (var, l) ->
    let annot = Map.find_exn ctx var in
    return (Var (var, l)) @@ Annot.var annot (var, l)
  | Let_rec (var, g1, g2) ->
    let var_annot =
      Annot.fix (fun var_annot ->
        (self ~ctx:(Map.set ctx ~key:var ~data:var_annot) g1).data)
    in
    let ctx = Map.set ctx ~key:var ~data:var_annot in
    let g1 = self ~ctx g1 in
    let g2 = self ~ctx g2 in
    return (Let_rec (var, g1, g2)) @@ g2.data
;;

let rec rules g =
  match g.content with
  | Empty | Fail | Tok _ | Var _ -> Var.Map.empty
  | Seq (g1, g2) | Alt (g1, g2) ->
    (* Hack -- assumes no equal variables *)
    Map.merge_skewed (rules g1) (rules g2) ~combine:(fun ~key:_ _ _ -> assert false)
  | Let_rec (var, g1, g2) -> Map.set (rules g2) ~key:var ~data:g1
  | Rep g -> rules g
;;

type t =
  { init_state : State.t
  ; actions : Action.Set.t State.Map.t
  ; rules : State.t Var.Map.t
  }
[@@deriving sexp]

let add states state action =
  Map.update states state ~f:(function
    | None -> Action.Set.singleton action
    | Some actions -> Set.add actions action)
;;

let compile states (g : (_, Annot.t) Grammar.t) ~accept =
  let init_state = State.create () in
  (* Add shift actions for each state in [first] *)
  let states =
    g.data.first
    |> Set.to_list
    |> List.fold_left ~init:states ~f:(fun states (tok, state) ->
         add states init_state (Shift (tok, state)))
  in
  (* Add accepting states for each state in [last] *)
  let states =
    g.data.last
    |> Set.to_list
    |> List.fold_left ~init:states ~f:(fun states (_, state) -> add states state accept)
  in
  (* If [expr.null] then add [init_state] to accepting states *)
  let states = if g.data.null then add states init_state accept else states in
  (* Add pair transitions *)
  let states =
    g.data.pair_set
    |> Set.to_list
    |> List.fold_left ~init:states ~f:(fun states ((_, from), (tok, to_)) ->
         add states from (Shift (tok, to_)))
  in
  init_state, states
;;

let compile_rule states var g =
  let init_state, states = compile states g ~accept:(Reduce var) in
  init_state, states
;;

let create (g : (unit, _) Grammar.t) =
  (* Annotate the grammar accordingly *)
  let lg = label g in
  let lag = annotate ~ctx:Var.Map.empty lg in
  (* Find "rules" (let-rec bindings) *)
  let rules = rules lag in
  (* Compile [rules] and [g] *)
  let states = State.Map.empty in
  let states, rules =
    Map.fold
      rules
      ~init:(states, Var.Map.empty)
      ~f:(fun ~key:var ~data:g (states, rules) ->
      let init_state, states = compile_rule states var g in
      states, Map.set rules ~key:var ~data:init_state)
  in
  let init_state, states = compile states lag ~accept:Accept in
  { init_state; actions = states; rules }
;;

module To_dot = struct
  type 'a state =
    { mutable id : int
    ; buffer : Buffer.t
    }

  let register state curr_state : string =
    let id = [%string "%{state.id#Int}"] in
    Buffer.add_string state.buffer id;
    Buffer.add_char state.buffer ' ';
    let label = Fmt.str "%a" State.pp curr_state in
    Buffer.add_string
      state.buffer
      [%string {|[style=filled, shape = oval, label = "%{label}"];|}];
    Buffer.add_char state.buffer '\n';
    state.id <- state.id + 1;
    id
  ;;

  let shift state ~from ~sym ~to_ =
    let label = Symbol.sexp_of_t sym |> Sexp.to_string_hum in
    Buffer.add_string state.buffer [%string {|%{from} -> %{to_} [label = "S %{label}"];|}];
    Buffer.add_char state.buffer '\n'
  ;;

  let reduce state ~from ~var ~to_ =
    let var = Var.sexp_of_t var |> Sexp.to_string_hum in
    Buffer.add_string state.buffer [%string {|%{from} -> %{to_} [label = "R %{var}"];|}];
    Buffer.add_char state.buffer '\n'
  ;;

  let follow state t =
    let table = Hashtbl.create (module Int) in
    let rec loop curr_state =
      match Hashtbl.find table curr_state with
      | Some me -> me
      | None ->
        let me = register state curr_state in
        Hashtbl.set table ~key:curr_state ~data:me;
        (match Map.find t.actions curr_state with
         | None -> ()
         | Some actions ->
           Set.iter actions ~f:(function
             | Accept -> ()
             | Reduce var ->
               let to_ = loop (Map.find_exn t.rules var) in
               reduce state ~from:me ~var ~to_
             | Shift (sym, next_state) ->
               let to_ = loop next_state in
               let _var =
                 match sym with
                 | Var var -> ignore (loop (Map.find_exn t.rules var) : string)
                 | Tok _ -> ()
               in
               shift state ~from:me ~to_ ~sym));
        me
    in
    loop t.init_state
  ;;
end

let to_dot t =
  let open To_dot in
  let state = { id = 0; buffer = Buffer.create 2042 } in
  let _root = follow state t in
  let contents = Buffer.contents state.buffer in
  [%string "digraph {\n %{contents}}"]
;;
