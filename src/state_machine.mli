open Core
open Grammar

module State : sig
  type t [@@deriving compare, sexp, hash]

  include Comparable.S with type t := t
end

module Symbol : sig
  type t =
    | Tok of Token.t
    | Var of Var.t
  [@@deriving sexp, compare, hash]

  include Comparable.S with type t := t
end

module Action : sig
  type t =
    | Shift of Symbol.t * State.t
    | Reduce of Var.t
    | Accept

  include Comparable.S with type t := t
end

type t =
  { init_state : State.t
  ; actions : Action.Set.t State.Map.t
  ; rules : State.t Var.Map.t
  }
[@@deriving sexp]

val create : (unit, _) Grammar.t -> t
val to_dot : t -> string
