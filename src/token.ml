open Core

module type S = sig
  type t [@@deriving sexp, hash, compare]

  val pp : t Fmt.t

  include Comparable.S with type t := t
end

module Label : sig
  type t = int [@@deriving sexp, hash, compare]

  val pp : t Fmt.t
  val create : unit -> t

  include Comparable.S with type t := t
end = struct
  include Int

  let create =
    let next = ref 0 in
    fun () ->
      Int.incr next;
      !next
  ;;
end

module Labelled (T : S) : S with type t = T.t * Label.t = struct
  module T = struct
    type t = T.t * Label.t [@@deriving sexp, hash, compare]

    let pp = Fmt.pair T.pp Label.pp
  end

  include T
  include Comparable.Make (T)
end

include (
  struct
    include Char

    let create c = c
  end :
    sig
      include S

      val create : char -> t
    end)
